
#include <stdint.h>

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

int x,y = 0;
char init_done=false;

#define queu_size 50
int touch_x_vect[queu_size];
int touch_y_vect[queu_size];
int touch_0_vect[queu_size];
char touch_head, touch_tail;
char read_now=false;

void start_timer0_100ms()
{
  cli();
  TCCR2A=0;
  TCCR2B=0;
  TIMSK2=0;
  
  TCNT2=255;
  read_now=true;
  
  TIMSK2 |= (1 << TOIE2);
  TCCR2B |= (1 << CS22)|(1 << CS21);
  sei();
  //Serial.println("Timer3 started");
}

ISR(TIMER2_OVF_vect)
{
  static char timer_counter = 0;
  static char read_couter = 0;
  
  if(timer_counter==23)
  {
    TCNT2 = 150;
    timer_counter++;
  }else if ((timer_counter==24)||(read_now==true))
  {
    //Serial.println("10 ms");
    
    timer_counter=0;
    read_now=false;

    y=readY();
    x=readX();
    if((x!=0)||(y!=0))
    {
      touch_x_vect[touch_head]=x;
      touch_y_vect[touch_head]=y;
      touch_0_vect[touch_head]=read_couter;
      touch_head++;
      touch_head= touch_head%queu_size;
      
      //Serial.print("\n");
      read_couter=0;
    }
    if((x==0)&&(y==0))
    {
      
      read_couter++;
    }
    if(read_couter>10)
    {
      TIMSK2=0;
      read_couter=0;
      timer_counter=0;
      attachInterrupt(1, test_int, LOW);
    }       
  }
  else
    timer_counter++;
}

void prepareX()
{
  pinMode(A0, INPUT);   // A0
  pinMode(A1, OUTPUT);    // A1
  pinMode(A2, INPUT);   // A2
  pinMode(A3, OUTPUT);   // A3
  digitalWrite(A1, LOW); // set A1 to GND
  digitalWrite(A3, HIGH);  // set A3 as 5V
}
int readX() // returns the value of the touch screen's X-axis
{
  int xr=0;
  
  prepareX();
  delay(1); // short delay is required to give the analog pins time to adjust to their new roles
  xr=analogRead(A2);
  
  return xr;
}
 
int readY() // returns the value of the touch screen's Y-axis
{
  int yr=0;
  pinMode(A0, OUTPUT);   // A0
  pinMode(A1, INPUT);    // A1
  pinMode(A2, OUTPUT);   // A2
  pinMode(A3, INPUT);   // A3
  digitalWrite(A0, LOW); // set A0 to GND
  digitalWrite(A2, HIGH);  // set A2 as 5V
  delay(1); // short delay is required to give the analog pins time to adjust to their new roles
  yr=analogRead(A1);
  return yr;
}

char attached_int = true;

void test_int()
{
  if(init_done)
  {
    Serial.print("TOUCH!!!\n");
  //attached_int=false;
  detachInterrupt(1); 
  start_timer0_100ms();
  }
  //Serial.print("TOUCH 2 !!!\n");
}

void setup(void) {
  Serial.begin(115200);

  Serial.print("start123");

  touch_head=0;
  touch_tail=0;
  start_timer0_100ms();
  //attachInterrupt(1, test_int, RISING);
  prepareX();
  delay(50);
  
  //interrupts();
  init_done=true;
  
}

void loop(void) {

  static int count=0;

  if(touch_head!=touch_tail)
  {
    Serial.print(touch_x_vect[touch_tail],DEC);
    Serial.print(",");
    Serial.print(touch_y_vect[touch_tail],DEC);
    Serial.print(",");
    Serial.println(touch_0_vect[touch_tail],DEC);
    touch_tail++;
    touch_tail=touch_tail%queu_size;
  }
}
