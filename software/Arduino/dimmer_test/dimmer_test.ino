
#include <stdint.h>

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#define queu_size 50
#define AC_LOAD 4    // Output to Opto Triac pin
#define timer75_magic 1200
#define timer10_magic 160
#define zero_detect_int 0
#define signature_serial 'D'

int x,y,x2 = 0;
char init_done=false;


int touch_x_vect[queu_size];
int touch_y_vect[queu_size];
int touch_0_vect[queu_size];
int touch_x2_vect[queu_size];
unsigned char touch_head, touch_tail;
char read_now=false;
volatile unsigned int dimming = 60;  // Dimming level (0-128)  0 = ON, 128 = OFF
volatile int counterx = 0;
int seconds =0;
char attached = 0;

struct serial_signature
{
    unsigned char signature;
    char pad;
    unsigned int package_size;
};


void start_timer1_75us(long int counter_dim)
{
  if(counter_dim ==0)
    return;
    
  //Serial.print("Timer1 start\n");
  cli();
  counterx= counter_dim;
  TCCR1A=0;
  TCCR1B=0;
  
  TCNT1=0;
  OCR1A = 1200;
  
  TIMSK1 |= (1 << OCIE1A);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);
  sei();
}

void start_timer1_10us()
{  
  
  
  TCCR1A=0;
  TCCR1B=0;
  
  TCNT1=0;
  OCR1A = 160;
  cli();
  TIMSK1 |= (1 << OCIE1A);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);
  sei();
}

void start_timer2_100ms()
{
  cli();
  TCCR2A=0;
  TCCR2B=0;
  TIMSK2=0;
  
  TCNT2=200;
  read_now=true;
  
  TIMSK2 |= (1 << TOIE2);
  TCCR2B |= (1 << CS22)|(1 << CS21);
  sei();
  //Serial.println("Timer3 started");
}

ISR(TIMER2_OVF_vect)
{
  static char timer_counter = 0;
  static char read_couter = 0;
  
  if(timer_counter==23)
  {
    TCNT2 = 150;
    timer_counter++;
  }else if ((timer_counter==24)||(read_now==true))
  {
    //Serial.println("10 ms");
    
    timer_counter=0;
    read_now=false;

    y=readY();
    x2=readZ();
    x=readX();
    
     if((x==0)&&(y==0))
    {
      
      read_couter++;
    }
    touch_x_vect[touch_head]=x;
      touch_y_vect[touch_head]=y;
      
      touch_x2_vect[touch_head]=x2;
      
    if((x!=0)||(y!=0))
    {
      read_couter=0;
    }
    
   
    if(read_couter>10)
    {
      TIMSK2=0;
      read_couter=0;
      timer_counter=0;
      prepareX2();
      attachInterrupt(1, test_int, LOW);
    }       
    touch_0_vect[touch_head]=read_couter;
    touch_head++;
    touch_head= touch_head%queu_size;
  }
  else
    timer_counter++;
}

void prepareX()
{
  pinMode(A0, INPUT);   // A0
  pinMode(A1, OUTPUT);    // A1
  pinMode(A2, INPUT);   // A2
  pinMode(A3, OUTPUT);   // A3
  digitalWrite(A1, LOW); // set A1 to GND
  digitalWrite(A3, HIGH);  // set A3 as 5V
}

void prepareX2()
{
  pinMode(A0, OUTPUT);   // A0
  pinMode(A1, INPUT);    // A1
  pinMode(A2, INPUT);   // A2
  pinMode(A3, OUTPUT);   // A3
  digitalWrite(A0, LOW); // set A1 to GND
  digitalWrite(A3, HIGH);  // set A3 as 5V
}

void prepareZ1()
{
  pinMode(A0, INPUT); // A0
  pinMode(A1, OUTPUT);    // A1
  pinMode(A2, OUTPUT);   // A2
  pinMode(A3, INPUT);   // A3
  digitalWrite(A1, LOW); // set A1 to GND
  digitalWrite(A2, HIGH);  // set A3 as 5V
}

int readZ() // returns the value of the touch screen's X-axis
{
  int zr=0;
  
  prepareZ1();
  delay(1); // short delay is required to give the analog pins time to adjust to their new roles
  zr=analogRead(A3);
  
  return zr;
}

int readX() // returns the value of the touch screen's X-axis
{
  int xr=0;
  
  prepareX();
  delay(1); // short delay is required to give the analog pins time to adjust to their new roles
  xr=analogRead(A2);
  
  return xr;
}
int readX2() // returns the value of the touch screen's X-axis
{
  int xr=0;
  
  prepareX2();
  delay(1); // short delay is required to give the analog pins time to adjust to their new roles
  xr=analogRead(A0);
  
  return xr;
}
 
int readY() // returns the value of the touch screen's Y-axis
{
  int yr=0;
  pinMode(A0, OUTPUT);   // A0
  pinMode(A1, INPUT);    // A1
  pinMode(A2, OUTPUT);   // A2
  pinMode(A3, INPUT);   // A3
  digitalWrite(A0, LOW); // set A0 to GND
  digitalWrite(A2, HIGH);  // set A2 as 5V
  delay(1); // short delay is required to give the analog pins time to adjust to their new roles
  yr=analogRead(A1);
  return yr;
}

char attached_int = true;

void test_int()
{
  if(init_done)
  {
    //Serial.print("TOUCH!!!\n");
  //attached_int=false;
  detachInterrupt(1); 
  start_timer2_100ms();
  }
  //Serial.print("TOUCH 2 !!!\n");
}

ISR(TIMER1_COMPA_vect)
{
  if (OCR1A == timer10_magic)
  {
    TIMSK1=0;
    TCCR1B=0;
    digitalWrite(AC_LOAD, LOW);
  }else if(counterx == 0)
  {
    digitalWrite(AC_LOAD, HIGH);   // Fire the TRIAC
    TIMSK1=0;
    TCCR1B=0;
//    Serial.print(seconds);
//    Serial.print("\n");
//    seconds++;
    //start_timer1_75us(dimming);
    start_timer1_10us();
  }
  else
  {
    counterx--;
    //Serial.print(".");
  }
}

//the interrupt function must take no parameters and return nothing
volatile int counter_zerocross=0;
void zero_crosss_int()  //function to be fired at the zero crossing to dim the light
{
  // Firing angle calculation : 1 full 50Hz wave =1/50=20ms 
  // Every zerocrossing thus: (50Hz)-> 10ms (1/2 Cycle) 
  // For 60Hz => 8.33ms (10.000/120)
  // 10ms=10000us
  // (10000us - 10us) / 128 = 75 (Approx) For 60Hz =>65

  //int dimtime = (75*dimming);    // For 60Hz =>65    
  //delayMicroseconds(dimtime);    // Wait till firing the TRIAC
  if(!init_done)
    return;
  if(TIMSK1!=0)
  {
    Serial.print("Timer 1 allready started\n");
    __asm__("nop\n\t");
  }
   else
   {
    //start_timer1_75us(dimming);
    __asm__("nop\n\t");
   }
  //digitalWrite(AC_LOAD, HIGH);   // Fire the TRIAC
//  delayMicroseconds(10);         // triac On propogation delay (for 60Hz use 8.33)
//  digitalWrite(AC_LOAD, LOW);    // No longer trigger the TRIAC (the next zero crossing will swith it off) TRIAC
  //Serial.print("INT\n");
  counter_zerocross++;
}

String inString = "";
char commanddone = false;

void serialEvent()
{
  while (Serial.available())
  {
    int inChar = Serial.read();
    //Serial.print((char)inChar);
    inString += (char)inChar;
    if (inChar == '\n')
    {
     commanddone = true;
     
    }
  }
}

void setup(void) {
  Serial.begin(115200);

  //Serial.print("start123");

  touch_head=0;
  touch_tail=0;
  start_timer2_100ms();
  //attachInterrupt(1, test_int, RISING);
  //prepareX();
  delay(50);
  
  //interrupts();
  pinMode(AC_LOAD, OUTPUT);// Set AC Load pin as output
  TIMSK1=0;

  attachInterrupt(zero_detect_int, zero_crosss_int, FALLING);
  init_done=true;
  //digitalWrite(AC_LOAD, HIGH);
}

void loop(void) {
  serial_signature sig;
  sig.signature=signature_serial;

  if(counter_zerocross>50)
   {
     Serial.print(counter_zerocross);
     Serial.print("\n");
     counter_zerocross=0;
        Serial.print("dimming:");
      Serial.print(dimming);
      Serial.print("\n");
   }
   
  if(touch_head!=touch_tail)
  {
//    Serial.print(touch_x_vect[touch_tail],DEC);
//    Serial.print(",");
//    Serial.print(touch_y_vect[touch_tail],DEC);
//    Serial.print(",");
//    Serial.print(touch_0_vect[touch_tail],DEC);
//    Serial.print(",");
//    Serial.println(touch_x2_vect[touch_tail],DEC);

    int touch_details[4];
    sig.pad='T';
    touch_details[0]=touch_x_vect[touch_tail];
    touch_details[1]=touch_y_vect[touch_tail];
    touch_details[2]=touch_0_vect[touch_tail];
    touch_details[3]=touch_x2_vect[touch_tail];

    sig.package_size = sizeof(int)*4;

    Serial.write((uint8_t *)&sig,sizeof(serial_signature));
    Serial.write((uint8_t *)touch_details, sig.package_size);
    touch_tail++;
    touch_tail=touch_tail%queu_size;
  }
  
   if (commanddone == true)
  {
    int index;
    index = inString.indexOf("dimmer:");
    if(index != -1)
    {
      dimming = inString.substring(7).toInt();
      
      if (dimming ==0)
      {
        cli();
        detachInterrupt(zero_detect_int);
        digitalWrite(AC_LOAD, HIGH);     
        attached = 0;
        sei();
      }
      else if (dimming >= 120)
      {
        cli();
        digitalWrite(AC_LOAD, LOW);
        detachInterrupt(zero_detect_int);
        attached = 0;
        sei();
        //Serial.print("@@@@@@@@@@@@@@\n");
      } else
      if (attached == 0)
      {
        attachInterrupt(zero_detect_int, zero_crosss_int, FALLING);
        attached = 1;
        //Serial.print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
      }
      
//      Serial.print("dimming:");
//        Serial.print(dimming);
//        Serial.print("\n");
    }

    commanddone = false;
    inString="";
    sig.pad='D';
    sig.package_size = sizeof(unsigned int);
    Serial.write((uint8_t *)&sig,sizeof(serial_signature));
    Serial.write((uint8_t *)&dimming, sig.package_size);
  }
}
