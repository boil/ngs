int AC_LOAD = 3;    // Output to Opto Triac pin
volatile long int dimming = 60;  // Dimming level (0-128)  0 = ON, 128 = OFF
volatile int counterx = 0;
int seconds =0;
char attached = 0;

#define timer75_magic 1200
#define timer10_magic 160
#define zero_detect_int 1

void start_timer1_75us(long int counter_dim)
{
  if(counter_dim ==0)
    return;
    
  //Serial.print("Timer1 start\n");
  cli();
  counterx= counter_dim;
  TCCR1A=0;
  TCCR1B=0;
  
  TCNT1=0;
  OCR1A = 1200;
  
  TIMSK1 |= (1 << OCIE1A);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);
  sei();
}

void start_timer1_10us()
{  
  
  
  TCCR1A=0;
  TCCR1B=0;
  
  TCNT1=0;
  OCR1A = 160;
  cli();
  TIMSK1 |= (1 << OCIE1A);
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS10);
  sei();
}

void setup()
{
  Serial.begin(9600); 
  Serial1.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  pinMode(AC_LOAD, OUTPUT);// Set AC Load pin as output
  //pinMode(20, INPUT);
  //digitalWrite(7, LOW);
  attachInterrupt(zero_detect_int, zero_crosss_int, FALLING);  // Choose the zero cross interrupt # from the table above
  Serial.print("start1\n");
  
  
  //digitalWrite(AC_LOAD, HIGH);
}

ISR(TIMER1_COMPA_vect)
{
  if (OCR1A == timer10_magic)
  {
    TIMSK1=0;
    TCCR1B=0;
    digitalWrite(AC_LOAD, LOW);
  }else if(counterx == 0)
  {
    digitalWrite(AC_LOAD, HIGH);   // Fire the TRIAC
    TIMSK1=0;
    TCCR1B=0;
//    Serial.print(seconds);
//    Serial.print("\n");
//    seconds++;
    //start_timer1_75us(dimming);
    start_timer1_10us();
  }
  else
  {
    counterx--;
    //Serial.print(".");
  }
}

//the interrupt function must take no parameters and return nothing
volatile int counter=0;
void zero_crosss_int()  //function to be fired at the zero crossing to dim the light
{
  // Firing angle calculation : 1 full 50Hz wave =1/50=20ms 
  // Every zerocrossing thus: (50Hz)-> 10ms (1/2 Cycle) 
  // For 60Hz => 8.33ms (10.000/120)
  // 10ms=10000us
  // (10000us - 10us) / 128 = 75 (Approx) For 60Hz =>65

  //int dimtime = (75*dimming);    // For 60Hz =>65    
  //delayMicroseconds(dimtime);    // Wait till firing the TRIAC
  if(TIMSK1!=0)
    Serial.print("Timer 1 allready started\n");
   else
    start_timer1_75us(dimming);
  //digitalWrite(AC_LOAD, HIGH);   // Fire the TRIAC
//  delayMicroseconds(10);         // triac On propogation delay (for 60Hz use 8.33)
//  digitalWrite(AC_LOAD, LOW);    // No longer trigger the TRIAC (the next zero crossing will swith it off) TRIAC
  //Serial.print("INT\n");
  counter++;
}

String inString = "";
char commanddone = false;

void serialEvent1()
{
  while (Serial1.available())
  {
    int inChar = Serial1.read();
    //Serial.print((char)inChar);
    inString += (char)inChar;
    if (inChar == '\n')
    {
     commanddone = true;
     
    }
  }
}

void loop()  {
//  for (int i=5; i <= 128; i++){
//    dimming=i;
//    delay(10);
//   }
   if(counter>50)
   {
     Serial.print(counter);
     Serial.print("\n");
     counter=0;
   }
   
  if (commanddone == true)
  {
    int index;
    index = inString.indexOf("dimmer:");
    if(index != -1)
    {
      dimming = inString.substring(7).toInt();
      
      if (dimming ==0)
      {
        cli();
        detachInterrupt(zero_detect_int);
        digitalWrite(AC_LOAD, HIGH);     
        attached = 0;
        sei();
        Serial.print("!!!!!!!!!!!!!!\n");
      }
      else if (dimming >= 120)
      {
        cli();
        digitalWrite(AC_LOAD, LOW);
        detachInterrupt(zero_detect_int);
        attached = 0;
        sei();
        Serial.print("@@@@@@@@@@@@@@\n");
      } else
      if (attached == 0)
      {
        attachInterrupt(zero_detect_int, zero_crosss_int, FALLING);
        attached = 1;
        Serial.print("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
      }
      
      Serial.print("dimming:");
      Serial.print(dimming);
      Serial.print("\n");
    }
    commanddone = false;
    inString="";
  }
}

