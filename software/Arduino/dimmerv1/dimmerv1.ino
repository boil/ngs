int AC_LOAD = 3;    // Output to Opto Triac pin
volatile int dimming = 3;  // Dimming level (0-128)  0 = ON, 128 = OFF
char attached = 0;


ISR(TIMER1_OVF_vect)
{
  //Serial.print("Bau2\n");
  //sei();
}

ISR(TIMER1_COMPA_vect)
{
  //Serial.print("Bau\n");
  //sei();
}

void setup()
{
  Serial.begin(9600); 
  Serial1.begin(115200);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  pinMode(AC_LOAD, OUTPUT);// Set AC Load pin as output
  attachInterrupt(1, zero_crosss_int, FALLING);  // Choose the zero cross interrupt # from the table above
  attached = 1;
  Serial.print("start5\n");
  //digitalWrite(AC_LOAD, HIGH);
  //sei();
  pinMode(9, OUTPUT);
  cli();
  
  
  TCCR1B=0;
  TCCR1C=0;
  TCNT1=0;
  OCR1A = 15624;
  TCCR1A= 0b11000000;
  //TCCR1A= 0b00000000;
  TCCR1B = 0b00010101;
  ICR1 = 23438;
  TIMSK1 |= (1 << TOIE1) | (1 << OCIE1A) | (1 << ICIE1);
  sei();
  //digitalWrite(AC_LOAD, HIGH);
}



//the interrupt function must take no parameters and return nothing
volatile int counter=0;
void zero_crosss_int()  //function to be fired at the zero crossing to dim the light
{
  // Firing angle calculation : 1 full 50Hz wave =1/50=20ms 
  // Every zerocrossing thus: (50Hz)-> 10ms (1/2 Cycle) 
  // For 60Hz => 8.33ms (10.000/120)
  // 10ms=10000us
  // (10000us - 10us) / 128 = 75 (Approx) For 60Hz =>65

  int dimtime = (75*dimming);    // For 60Hz =>65    
  delayMicroseconds(dimtime);    // Wait till firing the TRIAC
  digitalWrite(AC_LOAD, HIGH);   // Fire the 00TRIAC
  delayMicroseconds(10);         // triac On propogation delay (for 60Hz use 8.33)
  digitalWrite(AC_LOAD, LOW);    // No longer trigger the TRIAC (the next zero crossing will swith it off) TRIAC
  //Serial.print("Bau");
  counter++;
}

String inString = "";
char commanddone = false;

void serialEvent1()
{
  while (Serial1.available())
  {
    int inChar = Serial1.read();
    //Serial.print((char)inChar);
    inString += (char)inChar;
    if (inChar == '\n')
    {
     commanddone = true;
     
    }
  }
}



void loop()  {
//  for (int i=5; i <= 128; i++){
//    dimming=i;
//    delay(10);
//   }
   if(counter>200)
   {
     Serial.print(counter);
     Serial.print("\n");
     counter=0;
   }
  if (commanddone == true)
  {
    int index;
    index = inString.indexOf("dimmer:");
    if(index != -1)
    {
      Serial.print("!!!!!!!!!!!!!!!!!!!!!\n");
      Serial.print(inString.substring(7));
      Serial.print("!!!!!!!!!!!!!!!!!!!!!\n");
      dimming = inString.substring(7).toInt();
      if (dimming ==0)
      {
        digitalWrite(AC_LOAD, HIGH);
        detachInterrupt(1);
        attached = 0;
      }
      else if (attached == 0)
      {
        attachInterrupt(1, zero_crosss_int, FALLING);
        attached = 1;
      }
      Serial.print("dimming:");
      Serial.print(dimming);
      Serial.print("\n");
    }
    commanddone = false;
    inString="";
  }
}

