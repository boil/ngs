
// Touch screen library with X Y and Z (pressure) readings as well
// as oversampling to avoid 'bouncing'
// This demo code returns raw readings, public domain

#include <stdint.h>
#include "TouchScreen.h"

#define YP A2  // must be an analog pin, use "An" notation!
#define XM A3  // must be an analog pin, use "An" notation!
#define YM A1   // can be a digital pin
#define XP A0   // can be a digital pin

#define xLow  14
#define xHigh 15
#define yLow  16
#define yHigh 17
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
 

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate

int x,y = 0;


int readX() // returns the value of the touch screen's X-axis
{
  int xr=0;
  pinMode(A0, INPUT);   // A0
  pinMode(A1, OUTPUT);    // A1
  pinMode(A2, INPUT);   // A2
  pinMode(A3, OUTPUT);   // A3
  digitalWrite(A1, LOW); // set A1 to GND
  digitalWrite(A3, HIGH);  // set A3 as 5V
  delay(5); // short delay is required to give the analog pins time to adjust to their new roles
  xr=analogRead(A0); 
  return xr;
}
 
int readY() // returns the value of the touch screen's Y-axis
{
  int yr=0;
  pinMode(A0, OUTPUT);   // A0
  pinMode(A1, INPUT);    // A1
  pinMode(A2, OUTPUT);   // A2
  pinMode(A3, INPUT);   // A3
  digitalWrite(A0, LOW); // set A0 to GND
  digitalWrite(A2, HIGH);  // set A2 as 5V
  delay(5); // short delay is required to give the analog pins time to adjust to their new roles
  yr=analogRead(A1);
  return yr;
}

static volatile char flag;
static volatile int flag2=0;
//#define ANALOG_COMP_vect _VECTOR(23)

ISR(ANALOG_COMP_vect) {
 
    //with this test, ensure that indeed the interrupt result is 1 = V(AIN0) > V(AIN1)
    //(double check ?)
    if ( (ACSR & (1 << ACO))== 0 ) {
        flag=true;
        flag2++;
        }
      else
      {
      flag=false;
      }
      
      Serial.print("OUCH\n");
}
//
//void testfnct()
//{
//  Serial.print("OUCH\n"); 
//}

void setup(void) {
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  Serial.print("start123");
  noInterrupts();
  pinMode(A1, OUTPUT);
  pinMode(A3, OUTPUT);
  //pinMode(7,  INPUT);
  PORTE = 0;
  digitalWrite(A1, LOW);
  digitalWrite(A3, HIGH);
  sbi(ADCSRA,ADEN);
  cbi(ADCSRB,ACME);
  sbi(DIDR1, AIN0D);

  ACSR = 
  (0<<ACD) |   // Analog Comparator: Enabled
  (0<<ACBG) |   // Analog Comparator Bandgap Select: AIN0 is applied to the positive input
  (0<<ACO) |   // Analog Comparator Output: Off
  (1<<ACI) |   // Analog Comparator Interrupt Flag: Clear Pending Interrupt
  (1<<ACIE) |   // Analog Comparator Interrupt: Enabled
  (0<<ACIC) |   // Analog Comparator Input Capture: Disabled
  (1<<ACIS1) | (1<<ACIS0);   // Analog Comparator Interrupt Mode: Comparator Interrupt on Rising Output Edge
//    cbi(ADMUX, MUX0);
//  cbi(ADMUX, MUX1);
//  cbi(ADMUX, MUX2);
//analogComparator.enableInterrupt(testfnct, CHANGE);
//  analogComparator.setOn(AIN0, INTERNAL_REFERENCE);
  
  interrupts();
}

void loop(void) {

//    Serial.print(x,DEC);   
//    Serial.print(",");     
//    Serial.println(y,DEC); 
if(flag)
{
  Serial.print("OUCH2\n");
  Serial.print(flag2);
  flag=false;
}
 
  delay(50);
}
