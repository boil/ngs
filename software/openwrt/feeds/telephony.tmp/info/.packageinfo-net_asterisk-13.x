Source-Makefile: feeds/telephony/net/asterisk-13.x/Makefile
Package: asterisk13
Menu: 1
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread +jansson +libncurses +libopenssl +libpopt +libsqlite3 +libstdcpp +libuuid +libxml2 +libxslt +zlib
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Complete open source PBX, v13.2.0
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:  Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-alarmreceiver
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Alarm receiver support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Central Station Alarm receiver for Ademco Contact ID in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-authenticate
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Authenticate commands support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Execute arbitrary authenticate commands in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-directed_pickup
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Directed call pickup support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for directed call pickup in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-disa
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Direct Inward System Access support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Direct Inward System Access in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-exec
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Exec application support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for application execution in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-chanisavail
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Channel availability check support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for checking if a channel is available in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-chanspy
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Channel listen in support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for listening in on any channel in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-minivm
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Minimal voicemail system support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support a voicemail system in small building blocks working together based on the Comedian Mail voicemail in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-mixmonitor
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Record a call and mix the audio support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support record a call and mix the audio during the recording in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-originate
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Originate a call support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support originating an outbound call and connecting it to a specified extension or application in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-playtones
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Playtones application support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support play a tone list in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-read
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Variable read support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support a trivial application to read a variable in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-readexten
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Extension to variable support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support a trivial application to read an extension into a variable in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-record
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Record sound file support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support to record a sound file in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-sayunixtime
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Say Unix time support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support an application to say Unix time in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-senddtmf
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Send DTMF digits support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Sends arbitrary DTMF digits in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-sms
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-app-sms:libpopt +PACKAGE_asterisk13-app-sms:libstdcpp
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: SMS support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support SMS support (ETSI ES 201 912 protocol 1) in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-stack
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-app-stack:asterisk13-res-agi
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Stack applications support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Stack applications Gosub Return etc. in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-system
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: System exec support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for executing system commands in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-talkdetect
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: File playback with audio detect support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support for file playback with audio detect in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-verbose
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Verbose logging support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Verbose logging application in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-waituntil
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Sleep support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support sleeping until the given epoch in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-app-while
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: While loop support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support a while loop implementation in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-cdr
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Provides CDR support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Call Detail Record in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-cdr-csv
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Provides CDR CSV support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Call Detail Record with CSV support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-cdr-sqlite3
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 libsqlite3
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Provides CDR SQLITE3 support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Call Detail Record with SQLITE3 support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-a-mu
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Alaw to ulaw translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translation between alaw and ulaw codecs in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-adpcm
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: ADPCM text support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support ADPCM text  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-alaw
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Signed linear to alaw translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translation between signed linear and alaw codecs in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-ulaw
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Signed linear to ulaw translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translation between signed linear and ulaw codecs in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-g722
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: G.722 support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support a high bit rate 48/56/64Kbps ITU standard codec in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-g726
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Signed linear to G.726 translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translation between signed linear and ITU G.726-32kbps codecs in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-gsm
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: linear to GSM translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translate between signed linear and GSM in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-ilbc
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: linear to ILBC translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translate between signed linear and ILBC in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-lpc10
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Linear to LPC10 translation support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support translate between signed linear and LPC10 in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-codec-resample
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: resample sLinear audio support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support resample sLinear audio in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-curl
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-curl:libcurl
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: CURL support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support CURL support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-g726
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: G.726 support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for headerless G.726 16/24/32/40kbps data format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-g729
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: G.729 support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for raw headerless G729 data in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-gsm
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: GSM format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for GSM format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-h263
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: H263 format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for H264 format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-h264
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: H264 format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for H264 format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-ilbc
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: ILBC format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for ILBC format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-sln
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Raw slinear format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for raw slinear format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-pcm
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: PCM format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for PCM format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-vox
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: VOX format support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for ADPCM vox format in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-wav
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: WAV format (8000hz Signed Linear) support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for proprietary Microsoft WAV format (8000hz Signed Linear) in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-format-wav-gsm
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: WAV format (Proprietary GSM) support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for proprietary Microsoft WAV format (Proprietary GSM) in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-base64
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: base64 support support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support of base64 function in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-blacklist
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Blacklist on callerid support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support looking up the callerid number and see if it is blacklisted in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-cut
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: CUT function support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support CUT function in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-db
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Database interaction support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support functions for interaction with the database in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-devstate
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Blinky lights control support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support functions for manually controlled blinky lights in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-extstate
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Hinted extension state support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support retrieving the state of a hinted extension for dialplan control in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-global
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Global variable support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support global variable dialplan functions in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-groupcount
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Group count support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support for counting number of channels in the specified group in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-channel
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Channel info support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Channel info dialplan function in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-shell
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Shell support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for shell execution in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-uri
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: URI encoding and decoding support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Encodes and decodes URI-safe strings in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-func-vmcount
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: vmcount dialplan support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support a vmcount dialplan function in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-chan-iax2
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-chan-iax2:asterisk13-res-timing-timerfd
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: IAX2 channel support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support IAX support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-pjsip
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-pjsip:asterisk13-res-sorcery +PACKAGE_asterisk13-pjsip:libpjsip +PACKAGE_asterisk13-pjsip:libpjmedia +PACKAGE_asterisk13-pjsip:libpjnath +PACKAGE_asterisk13-pjsip:libpjsip-simple +PACKAGE_asterisk13-pjsip:libpjsip-ua +PACKAGE_asterisk13-pjsip:libpjsua +PACKAGE_asterisk13-pjsip:libpjsua2
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: pjsip channel support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support the channel pjsip in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-chan-skinny
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Skinny channel support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support the channel chan_skinny in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-chan-sip
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: SIP channel support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support the channel chan_sip in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-chan-unistim
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Unistim channel support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support channel driver for the UNISTIM (Unified Networks IP Stimulus) protocol in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-odbc
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-odbc:libpthread +PACKAGE_asterisk13-odbc:libc +PACKAGE_asterisk13-odbc:unixodbc
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: ODBC support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support ODBC support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-pgsql
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-pgsql:libpq
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: PostgreSQL support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support PostgreSQL support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-pbx-ael
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Asterisk Extension Logic support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for symbolic Asterisk Extension Logic in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-pbx-spool
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Call Spool support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support outgoing call spool support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-ael-share
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Shareable AEL code support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support support for shareable AEL code mainly between internal and external modules in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-agi
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Asterisk Gateway Interface support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Support for the Asterisk Gateway Interface extension in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-clioriginate
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Calls via CLI support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Originate calls via the CLI in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-monitor
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Provide Monitor support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Cryptographic Signature capability in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-phoneprov
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Phone Provisioning support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Phone provisioning application for the asterisk internal http server in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-smdi
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Provide SMDI support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Simple Message Desk Interface capability in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-fax
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-res-fax:asterisk13-res-timing-pthread
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: FAX modules support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Generic FAX resource for FAX technology resource modules in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-musiconhold
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: MOH support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support Music On Hold support in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-http-websocket
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: HTTP websocket support support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-rtp-asterisk
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13 +PACKAGE_asterisk13-res-rtp-asterisk:libpjsip +PACKAGE_asterisk13-res-rtp-asterisk:libpjmedia +PACKAGE_asterisk13-res-rtp-asterisk:libpjnath +PACKAGE_asterisk13-res-rtp-asterisk:libpjsip-simple +PACKAGE_asterisk13-res-rtp-asterisk:libpjsip-ua +PACKAGE_asterisk13-res-rtp-asterisk:libpjsua +PACKAGE_asterisk13-res-rtp-asterisk:libpjsua2
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: RTP stack support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-rtp-multicast
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: RTP multicast engine support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-sorcery
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Sorcery data layer support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-timing-pthread
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: pthread Timing Interface support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-res-timing-timerfd
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Timerfd Timing Interface support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support  in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@

Package: asterisk13-voicemail
Submenu: Telephony
Version: 13.2.0-1
Depends: +libc +USE_EGLIBC:librt +USE_EGLIBC:libpthread asterisk13
Conflicts: 
Menu-Depends: 
Provides: 
Build-Depends: libxml2/host
Section: net
Category: Network
Title: Voicemail support
Maintainer: Jiri Slachta <slachta@cesnet.cz>
Source: asterisk-13.2.0.tar.gz
License: GPL-2.0
LicenseFiles: COPYING LICENSE
Type: ipkg
Description:    Asterisk is a complete PBX in software. It provides all of the features
 you would expect from a PBX and more. Asterisk does voice over IP in three
 protocols, and can interoperate with almost all standards-based telephony
 equipment using relatively inexpensive hardware.
This package provides support voicemail related modules in Asterisk.
http://www.asterisk.org/
Jiri Slachta <slachta@cesnet.cz>
@@


