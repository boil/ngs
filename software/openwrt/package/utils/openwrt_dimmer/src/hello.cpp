#include "mosquitto_com.hpp"
#include "communication.hpp"
#include "serial_com.hpp"
#include "basicmessage.hpp"
#include "touchscreen.hpp"
#include <termios.h>
#include <thread>
#include <sstream>
#include <cstdlib>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <fstream>

#define TOUCH_PIPE "/tmp/myfifo"
extern int init_interface();

//returns the dimmer value compatible with AVR IN: dimm intensity from 0-100
//returns -1 if no change in dimm_value occured; a value between 0-120 if a change was made
int prepare_dimm_value(int dimm_value)
{
	static int current_dimm_value=0;
	static int old_dimm_value=0;
	
	if (abs(current_dimm_value - dimm_value)<4)
		return -1; //ignore small changes
		
	old_dimm_value = current_dimm_value;
	current_dimm_value = dimm_value;
	
	return abs(current_dimm_value -120);
}

int main()
{
	//load kernel modules here
	system("insmod fbtft >> /tmp/log.txt");
	system("insmod fb_ili9341 >> /tmp/log.txt");
	system("insmod fbtft_device >> /tmp/log.txt");
	//create the named pipe to send touch screen events to nanox
	mkfifo(TOUCH_PIPE, 0666);
	int touch_pipe;
	touch_pipe = open(TOUCH_PIPE, O_RDWR,O_NONBLOCK);
	
	std::cout<<"Opened pipe!!!\n";
	
	std::map<std::string,std::string> wifi_map;
	wifi_map.insert(std::pair<std::string,std::string>("WPA PSK (CCMP)","psk"));
	wifi_map.insert(std::pair<std::string,std::string>("WPA PSK (TKIP)","psk+tkip"));
	wifi_map.insert(std::pair<std::string,std::string>("WPA PSK (TKIP, CCMP)","psk+tkip+ccmp"));
	wifi_map.insert(std::pair<std::string,std::string>("WPA2 PSK (CCMP)","psk2"));
	wifi_map.insert(std::pair<std::string,std::string>("WPA2 PSK (TKIP)","psk2+tkip"));
	wifi_map.insert(std::pair<std::string,std::string>("WPA2 PSK (TKIP, CCMP)","psk2+tkip+ccmp"));
	wifi_map.insert(std::pair<std::string,std::string>("mixed WPA/WPA2 PSK (CCMP)","mixed-psk+ccmp"));
	wifi_map.insert(std::pair<std::string,std::string>("mixed WPA/WPA2 PSK (TKIP)","mixed-psk+ccmp"));
	wifi_map.insert(std::pair<std::string,std::string>("mixed WPA/WPA2 PSK (TKIP, CCMP)","mixed-psk"));
	
	
	
    SafeQueue <BasicMessage> main_queu;
    
    pid_t childPID;

    childPID = fork();

    if(childPID >= 0) // fork was successful
    {
        if(childPID == 0) // child process
        {
            std::cout<<"\n Child Process OPENED!!! \n";
            init_interface();
            std::cout<<"\n Child Process OUT!!! \n";
            return 0;
        }
        else //Parent process
        {
            std::cout<<"\n Parent process\n";
        }
    }
    else // fork failed
    {
        std::cout<<"\n Fork failed, quitting!!!!!!\n";
        return 1;
    }
    
    std::cout<<"EXECUTING PARENT CODE!!!!\n";
    
    //run nano-X server   
    childPID = fork();
    if(childPID >= 0) // fork was successful
    {
        if(childPID == 0) // child process
        {
            std::cout<<"\n Child Process OPENED nano-X!!! \n";
            sleep(2);
            execl("/usr/bin/nano-X", "nano-X", (char*)0);
            return 0;
        }
        else //Parent process
        {
            std::cout<<"\n Parent process\n";
        }
    }
    else // fork failed
    {
        std::cout<<"\n Fork failed, quitting!!!!!!\n";
        return 1;
    }
    std::cout<<"EXECUTING PARENT CODE!!!!\n";
    mosquitto_lib_init();

    std::cout << "main " << std::this_thread::get_id() << "\n";
    //std::cout << "B115200 "<< std::to_string(B115200) << "\n";
    std::ostringstream ss;
    ss << B9600;
    std::cout << "B9600 "<< ss.str() << "\n";



    SerialConnection test_serial;
#ifdef FOR_ARM
    test_serial.init(std::map<std::string,std::string>({{"device","/dev/ttyATH0"},{"speed",ss.str()}}));
#else
    test_serial.init(std::map<std::string,std::string>({{"device","/dev/ttyACM0"},{"speed",ss.str()}}));
#endif
    test_serial.subscribe_queu(&main_queu);
    //if(test_serial.connect() != 0)
    //{
    //    std::cout << "Error oppening serial\n";
   // }

    MosquittoConnection test("otside_dimmer");
    test.init(std::map<std::string,std::string>({{"address","iot.eclipse.org"},{"port","1883"}}));
    test.subscribe_queu(&main_queu);
    test.add_topic("topic/dimm_val");
    test.connect();

    MosquittoConnection local_mosq("local_dimmer");
    local_mosq.init(std::map<std::string,std::string>({{"address","127.0.0.1"},{"port","1883"}}));
    local_mosq.subscribe_queu(&main_queu);
    local_mosq.add_topic("topic/wifi_setup");
    local_mosq.add_topic("/local/progressbar");
    local_mosq.connect();
    

	int count_touches=0;
	
    while(true)
    {
            BasicMessage tmp_msg = main_queu.dequeue_block();
            std::string s = tmp_msg.info_str["type"];
            std::string funtion_str = tmp_msg.info_str["function"];

			//printf("blablablablabvalblablabalbalbalbablablablblabla: %s\n", s.c_str());
            if (s=="serial")
            {
				
            	if(funtion_str == "dimmer")
            	{
            		unsigned short int *dimm;
            		dimm = (unsigned short int *)tmp_msg.get_data_point();
            		//	SwapEndian(*dimm);
            		std::cout<<"RECEIVED DIMM:"<<*dimm<<"\n";
            	}

            	if(funtion_str == "touch")
            	{
					unsigned short int *coords;
            		int pressure;
            		coords= (unsigned short int *)tmp_msg.get_data_point();
            		touchscreen_point temp_point;
#ifdef FOR_ARM            		
            		SwapEndian(coords[0]);
            		SwapEndian(coords[1]);
            		SwapEndian(coords[2]);
#endif            		
            		temp_point.X = coords[0];
            		temp_point.Y = coords[1];
            		temp_point.zero_counter = coords[2];
            		if(coords[3]>0)
            			//temp_point.pressure = 600*temp_point.X/1024*(1024/coords[3] -1)- 320*(1-temp_point.Y/1024);
            			pressure= abs(300*(double)temp_point.X/1024*((double)1024/coords[3] -1)- 540*(1-(double)temp_point.Y/1024));
            		else
            			//temp_point.pressure =0;
            			pressure=0;
					
            		temp_point.pressure=pressure;
            		//std::cout<<"X: "<<temp_point.X<<" Y: "<<temp_point.Y<<" P: "<<temp_point.pressure<< " coords[3]: "<<coords[3]<<"\n";
            		if (pressure<=0)
            		{
						count_touches++;
						if(count_touches>2)
							continue;
					}
					else
						count_touches=0;
						
					if(-1==write(touch_pipe, &temp_point, sizeof(touchscreen_point)))
						std::cout<<"ERROR!!! Could not write to touch screen pipe!!!!\n";
					
				}
			}
            if (s=="mosquitto")
            {
            	std::string topic_str = tmp_msg.info_str["topic"];
            	if(topic_str=="topic/dimm_val")
            	{
            		BasicMessage msg;
            		msg.add_info(std::map<std::string, std::string> ({{"type", "raw"}}));
            		msg.put_data(tmp_msg.get_data_point(), tmp_msg.get_size());
            		std::cout<<"mosquitto: "<<tmp_msg.get_data_point()<<"\n";
            		
            		char * tmp_chr = strchr(tmp_msg.get_data_point(),':');
            		if(tmp_chr!=NULL)
            		{
						BasicMessage msg_interface;
						msg_interface.add_info(std::map<std::string, std::string> ({{"topic", "/local/new_dimm_value"}}));
						msg_interface.put_data(tmp_chr+1, strlen(tmp_chr));
						local_mosq.send_message(msg_interface);
						test_serial.send_message(msg);
					}
						
//            		if(strstr(tmp_msg.get_data_point(),"quit"))
//            			break;
            	}else if (topic_str=="topic/wifi_setup")
            	{
					char *SSID = tmp_msg.get_data_point()+5;
					
					char *pass = strstr(SSID, "pass:");
					*(pass-1)=0;
					pass+=5;
					std::cout<<"Got SSID:"<<SSID<<" pass:"<<pass<"\n";
					system("iwinfo wlan0 scan > /tmp/wifi.list");
					std::ifstream t("/tmp/wifi.list");
					char *tmp_line= new char[1024];
					char *security;
					char *channel;
					char *quality;
					char found_wifi=false;
					
					while(t.getline(tmp_line, 1024))
					{
						if(strstr(tmp_line, SSID))
						{
							std::cout<<"FOUND SSID!!!!"<< tmp_line<<"\n";
							t.getline(tmp_line, 1024);
							channel = strdup(strstr(tmp_line, "Channel:"));
							t.getline(tmp_line, 1024);
							quality = strdup(tmp_line);
							t.getline(tmp_line, 1024);
							security = strdup(strstr(tmp_line, "Encryption: ")+strlen("Encryption: "));
							found_wifi=true;
							break;
						}
					}
					
					if(found_wifi)
					{
						std::cout<<"FOUND wifi "<< channel << " quality: "<< quality<< " security: "<<security<<"\n";
						std::string tmp_string(security);
						//std::map<std::string,std::string >::iterator it;
						auto it = wifi_map.find(tmp_string);
						if(it!=wifi_map.end())
						{
							std::cout<<"FOUND security "<<it->second<<"\n";
							std::stringstream command_string;
							
							command_string << "uci set wireless.@wifi-iface[0].ssid=\'"<<SSID<<"\'";
							//command_string +=
							std::cout<<"exec commad:";
							std::cout<<command_string.rdbuf()<<"\n"<<std::flush;
						}
					}
					
					t.close();
					delete[] tmp_line;
					

            	}else if (topic_str=="/local/progressbar")
            	{
					int *dimm_val = (int*)tmp_msg.get_data_point();
					std::cout<<"/local/progressbar: "<<*dimm_val<<"\n";
				}

            }
            //std::cout<<"From MAIN: "<<s<<" topic: "<<tmp_msg.info_str["type"]<<" data: "<< tmp_msg.info_str["function"]<<"\n";
    }
    close(touch_pipe);
    test.disconnect();
    std::cout<<"OUT\n;";
}
