#include "communication.hpp"

void CommunicationGeneral::init(std::map<std::string, std::string>&& option)
{
    connection_optons = option;
    run_loop = false;
}

Error_ID CommunicationGeneral::connect()
{
    run_loop = true;
    std::thread t(&CommunicationGeneral::connection_thred_function, this);
    connection_thred = std::move(t);
    return NOERROR;
}


bool CommunicationGeneral::disconnect()
{
    run_loop = false;
    connection_thred.join();
    if(disconnect_resource()!=0)
        return false;
    else
        return true;
}

bool CommunicationGeneral::pause_connection()
{
    pauseconnection = true;
    return pauseconnection;
}

bool CommunicationGeneral::unpause_connection()
{
    pauseconnection = false;
    return pauseconnection;
}

void CommunicationGeneral::connection_thred_function()
{
	std::chrono::milliseconds dura( 10 );
    std::cout << "connection_thred_function " << std::this_thread::get_id() << "\n";

    int return_error = connect_resource();
    if (return_error != 0)
    {
    	std::cout<<"Connect error: "<<return_error<<"\n";
    	connection_established = false;
    	if (!auto_retry_connect)
    		return;
    }
    else
    	connection_established = true;

    while(!connection_established && run_loop)
    {
		std::chrono::milliseconds reconnect_dura( 5000 );
    	//std::cout<<"TRY TO RECONNECT!!!!!\n";
    	 std::this_thread::sleep_for( reconnect_dura );
    	int return_error = connect_resource();
    	if (return_error == 0)
    	{
    		connection_established = true;
    		std::cout<< "connection_established = true\n";
    	}
    }

    while(run_loop)
    {



        std::this_thread::sleep_for( dura );

        if(!commads_queu.is_empty())
        {
        	if (connection_established)
        	{
        		BasicMessage send_msg(commads_queu.dequeue_block());
        		write_resource(send_msg);
        	}
        }

        if(pauseconnection)
        {
           continue;
        }
        BasicMessage message_received(connection_loop());
        int x=message_received.get_size();
        if(x>0)
        {
            for(auto sub_queu:subscribed_queus)
                sub_queu->enqueue(std::move(message_received));
        }

    }
}

bool CommunicationGeneral::send_message(BasicMessage &msg)
{
    commads_queu.enqueue(msg);
    return true;
}
