/*
 * touchscreen.hpp
 *
 *  Created on: Dec 27, 2014
 *      Author: boil
 */

#ifndef TOUCHSCREEN_HPP_
#define TOUCHSCREEN_HPP_

#define serial_signiture_byte 'D'
#define touch_tap_distance 20
#define touch_increase_distance 11

struct serial_signature
{
    unsigned char signiture;
    char pad;
    unsigned short int package_size;
};

struct touchscreen_point
{
	unsigned short int X;
	unsigned short int Y;
	unsigned short int zero_counter;
	int pressure;
};

struct touchscreen_gestures
{
	touchscreen_point last_point;
	int gesture_distance;
	int number_of_taps;
	int number_of_points;
	bool wait_for_touch;
};

#endif /* TOUCHSCREEN_HPP_ */
