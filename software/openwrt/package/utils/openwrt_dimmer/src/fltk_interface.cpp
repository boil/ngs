#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Slider.H>

#include <unistd.h>
#include "mosquitto_com.hpp"
#include "communication.hpp"
#include "basicmessage.hpp"
#include <sstream>
#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>

SafeQueue <BasicMessage> main_queu;
MosquittoConnection local_mosq("local_fltk");
Fl_Slider *dimmer_slider;

static void cb_PUSH(Fl_Button* button, void*) {
  printf("PUSHED BUTTON!!!! %d\n", button->value());
}

static void cb_PUSH2(Fl_Slider* sld, void*) {
	int progress_val = (int)(sld->value()*100);
  //printf("PUSHED SLIDER!!!! %d\n", progress_val);
  BasicMessage msg;
  msg.add_info(std::map<std::string, std::string> ({{"topic", "/local/progressbar"}}));
  //std::ostringstream ss;
  //ss << progress_val;
  //msg.put_data(ss.str().data(), ss.str().length()+1);
  msg.put_data((char*)&progress_val,sizeof(int));
  local_mosq.send_message(msg);
}

//Fl::awake(do_change_progress, NULL);

Fl_Double_Window* make_window() {
  Fl_Double_Window* w;
  { Fl_Double_Window* o = new Fl_Double_Window(0,0,240, 320);
    w = o;
    
    { Fl_Button* o = new Fl_Button(10, 270, 85, 40, "PUSH");
      o->callback((Fl_Callback*)cb_PUSH);
    } // Fl_Button* o
	{ Fl_Slider* o = new Fl_Slider(160, 10, 70, 300);
      o->callback((Fl_Callback*)cb_PUSH2);
      dimmer_slider = o;
    } // Fl_Slider* o
    
    o->clear_border();
   
    o->end();
  } // Fl_Double_Window* o
  return w;
}

std::mutex fltk_mutex;
std::condition_variable fltk_cond;

void do_change_progress(void *data)
{
	if((dimmer_slider!=NULL)&&(data))
	{
		int value = atoi((char*)data);
		std::cout<<"setting progress to:"<<value<<" from: "<<(char*)data<<"\n";
		dimmer_slider->value((double)value/100);
	}
	
	std::unique_lock<std::mutex> lock(fltk_mutex);
	fltk_cond.notify_one();
}

void fltk_thred_function()
{
	while(true)
	{
		BasicMessage tmp_msg = main_queu.dequeue_block();
		std::string msg_type = tmp_msg.info_str["type"];
		if(msg_type=="mosquitto")
		{
			std::string topic_str = tmp_msg.info_str["topic"];
			if(topic_str=="/local/new_dimm_value")
			{
				char *tmp_point = tmp_msg.get_data_point();
				std::cout<<"bla: "<<tmp_point<<"\n";
				if(Fl::awake(do_change_progress, (void*)tmp_point)==0)
				{
					std::unique_lock<std::mutex> lock(fltk_mutex);
					fltk_cond.wait(lock);
				}
			}
		}
	}
}

int init_interface() {
	
	//communication init
	
    local_mosq.init(std::map<std::string,std::string>({{"address","127.0.0.1"},{"port","1883"}}));
    local_mosq.subscribe_queu(&main_queu);
    local_mosq.add_topic("/local/new_dimm_value");
    local_mosq.connect();
    
    std::thread mosquitto_task(&fltk_thred_function);
	
     Fl_Double_Window *win=make_window();
     win->end();
	 win->show(0, NULL);

	 Fl::lock();
     return(Fl::run());
}
