#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Progress.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Slider.H>
//#ifdef FOR_ARM
//#include <microwin/nano-X.h>
//#endif

#include <unistd.h>
#include <pthread.h>

Fl_Progress *progress_bar=NULL;

void do_change_progress(void *data)
{
	if(progress_bar!=NULL)
	{
		int val=progress_bar->value();
		if (val >= 100)
			val=0;
		else
			val+=10;
		printf("set val: %d\n", val);
		progress_bar->value(val);
	}
}

void *change_progress(void *stuff)
{
	while(true){
	
	Fl::awake(do_change_progress, NULL);
	sleep(1);
	}
	return NULL;
}

static void cb_PUSH(Fl_Button*, void*) {
  printf("PUSHED BUTTON!!!!\n");
}

Fl_Double_Window* make_window() {
  Fl_Double_Window* w;
  { Fl_Double_Window* o = new Fl_Double_Window(0,0,240, 320);
    w = o;
    { Fl_Progress* o = new Fl_Progress(0, 0, 200, 25);
      o->selection_color((Fl_Color)81);
       o->value(100);
       progress_bar = o;
    } // Fl_Progress* o
    
    //{ Fl_Button* o = new Fl_Button(50, 25, 85, 40, "PUSH");
      //o->callback((Fl_Callback*)cb_PUSH);
    //} // Fl_Button* o
	{ Fl_Slider* o = new Fl_Slider(50, 30, 50, 235);
      o->callback((Fl_Callback*)cb_PUSH);
    } // Fl_Slider* o
    
    o->clear_border();
   
    o->end();
  } // Fl_Double_Window* o
  return w;
}

int main(int argc, char **argv) {
	
	
     Fl_Double_Window *win=make_window();
     if (progress_bar!=NULL)
		progress_bar->value(70);
     win->end();
	 win->show(argc, argv);
	 
	 pthread_t progress_thread;
	pthread_create(&progress_thread, NULL, change_progress, NULL);
	 Fl::lock();
     return(Fl::run());
}
