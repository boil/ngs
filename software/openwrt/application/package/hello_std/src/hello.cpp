#include <iostream>
#include <memory>
#include <vector>

using namespace std;

int main(void)
{
	shared_ptr<int> p3 (new int(5));
	vector<int> vec;
	vec.push_back(10);
	vec.push_back(20);
	
	for (auto i:vec)
	cout << i << " ";
  cout << "hello world - C++" << endl;
  return 0;
}
