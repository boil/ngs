package com.example.ngshome;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import android.widget.TextView;

public final class Dispatcher implements MqttCallback {

	
	public static Dispatcher GetInstance( )
	{
		if ( null == instance )
		{
			instance = new Dispatcher();
		}
		
		
		return instance;
	}
	
	public void Connect( )
	{
		try 
    	{
			instance.client = new MqttClient("tcp://192.168.0.100:9999", UUID.randomUUID().toString().substring(0, 10), new MemoryPersistence() );
			
			MqttConnectOptions conOptions = new MqttConnectOptions();
			
			conOptions.setCleanSession(Boolean.TRUE);
			
			instance.client.setCallback( instance );
			
			instance.connectionprogress.setText("Connecting...");
			
			instance.client.connect(conOptions);
			
		}
    	catch (Exception e) 
		{
            
    		instance.connectionprogress.setText("Could not connect ");
		}		
	}
	public void DisplayConnectionStatus( TextView view )
	{
		connectionprogress = view;
	}
	
	public Boolean Subscribe( String topic )
	{
		try 
		{
			client.subscribe(topic, 1);
		} 
		catch (Exception e) 
		{	
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	public Boolean Publish( String topic, String message )
	{
		MqttMessage mqttmessage = new MqttMessage();
		
		mqttmessage.setPayload( message.getBytes() );
		mqttmessage.setQos(1);
		mqttmessage.setRetained(Boolean.FALSE);
		
		try 
		{
			client.publish( topic, mqttmessage);
		} 
		catch (Exception e) 
		{
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	@Override
	public void connectionLost(Throwable arg0) 
	{
		connectionprogress.setText("Connection lost");
		Connect();
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) 
	{
		connectionprogress.setText("Message send" + arg0 );
	}

	@Override
	public void messageArrived(String arg0, MqttMessage arg1) throws Exception 
	{
		connectionprogress.setText("Message received " + arg0);

	}
	
	private Dispatcher()
	{
		
	}
	
	private static Dispatcher 	instance;
	
	private MqttClient			client;

	private TextView			connectionprogress;
}
