package com.example.ngshome;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;


public class MainActivity extends Activity {

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        if (savedInstanceState == null) 
        {
        	Fragment fragment = new PlaceholderFragment();
        	
            getFragmentManager().beginTransaction()
                    .add(R.id.container, fragment )
                    .commit();
            
        }
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.general_settings) 
        {
            return true;
        }
        if (id == R.id.privacy_settings) 
        {
            return true;
        }
        if (id == R.id.home_settings) 
        {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
    	
    	private SeekBar 		dimmercontrol;
    	private TextView		progress;
    	private TextView		connectionprogress;
    	private ToggleButton	dimmeractivate;
    	
        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        	
        	View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        	
        	dimmercontrol 			= (SeekBar)  		rootView.findViewById(R.id.dimmercontrol);
        	dimmeractivate			= (ToggleButton)	rootView.findViewById(R.id.dimmeractivate);
        	progress 				= (TextView) 		rootView.findViewById(R.id.progress );
        	connectionprogress 		= (TextView) 		rootView.findViewById(R.id.connectionprogress );
        	
        	progress.setText(Integer.toString(dimmercontrol.getProgress()));
        	
        	
        	Dispatcher.GetInstance().DisplayConnectionStatus(connectionprogress);
        	
        	Dispatcher.GetInstance().Connect();
        	
        	Dispatcher.GetInstance().Subscribe("dimmer.control.update");
        	
        	dimmeractivate.setOnClickListener(
        			new OnClickListener() {

						@Override
						public void onClick(View v) 
						{
							if ( View.VISIBLE == dimmercontrol.getVisibility() )
							{
								dimmercontrol.setVisibility(View.INVISIBLE);
								Dispatcher.GetInstance().Publish("dimmer.control.activate", Integer.toString(0) );
							}
							else
							{
								dimmercontrol.setVisibility(View.VISIBLE);
								Dispatcher.GetInstance().Publish("dimmer.control.activate", Integer.toString(1) );
							}
							
						}
        				
        			});
        	dimmercontrol.setOnSeekBarChangeListener(
                     new OnSeekBarChangeListener() {
         
     	    @Override
     	    public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) 
     	    {
     	    	progress.setText(Integer.toString(progresValue));
     	    	
     	    	Dispatcher.GetInstance().Publish("dimmer.control.value", Integer.toString(progresValue) );
     	    }
     	
     	    @Override
     	    public void onStartTrackingTouch(SeekBar seekBar) 
     	    {
     	    	
     	    }
     	
     		@Override
     		public void onStopTrackingTouch(SeekBar seekBar) 
     		{
     			
     		}
                     } );
        	 
        
            return rootView;
        }
        
    }

}
