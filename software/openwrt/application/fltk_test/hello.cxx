#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <microwin/nano-X.h>

int main(int argc, char **argv) {
  Fl_Window *window = new Fl_Window(0,0,200,150);
  Fl_Box *box = new Fl_Box(10,10,180,100,"Hello, World!");
  box->box(FL_UP_BOX);
  //box->labelsize(10);
  window->end();
  window->show(argc, argv);
  return Fl::run();
}
