import os
import time

dimmer = 10

while(1):
	os.system("mosquitto_pub -h test.mosquitto.org -p 1883 -t 'topic/test/1' -m dimmer:{0}".format(dimmer))
	time.sleep(0.5)
	dimmer += 5
	if (dimmer > 110):
		dimmer = 10
