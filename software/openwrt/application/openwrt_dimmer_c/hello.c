#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <errno.h>        
#include <stdlib.h>
#include <mosquitto.h>
#include <netinet/in.h>
#include <arpa/nameser.h>
#include <resolv.h>

#define BAUDRATE B115200 
#define MODEMDEVICE "/dev/ttyATH0"/*UART NAME IN PROCESSOR*/
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1
void openport(void);
int fd=0, n;
static int cnt, size, s_cnt;
unsigned char *var;
struct termios oldtp, newtp;
char sendcmd1[10]="\0";
FILE *file;

void openport(void)
{
         
	 fd = open(MODEMDEVICE, O_RDWR | O_NOCTTY |O_NDELAY );
	 printf("oviya :%d\n",fd);
         if (fd <0)
         {
         	perror(MODEMDEVICE);
 
         }
                                                                                
         fcntl(fd,F_SETFL,0);
         tcgetattr(fd,&oldtp); /* save current serial port settings */
    //     tcgetattr(fd,&newtp); /* save current serial port settings */
         bzero(&newtp, sizeof(newtp));
  //       bzero(&oldtp, sizeof(oldtp));
		newtp.c_cflag &= ~PARENB;
		newtp.c_cflag &= ~CSTOPB;
		newtp.c_cflag &= ~CSIZE;
		newtp.c_cflag &= ~CRTSCTS;
                                                                                
         newtp.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
                                                                                
         newtp.c_iflag = IGNPAR | ICRNL;
         newtp.c_iflag &= ~(IXON | IXOFF | IXANY);
                                                                                
         newtp.c_oflag = 0;                                                                        
         newtp.c_lflag = (ICANON | ECHO | ECHOE);                                                                    
         newtp.c_cc[VINTR]    = 0;     /* Ctrl-c */
         newtp.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
         newtp.c_cc[VERASE]   = 0;     /* del */
         newtp.c_cc[VKILL]    = 0;     /* @ */
        // newtp.c_cc[VEOF]     = 4;     /* Ctrl-d */
         newtp.c_cc[VTIME]    = 0;     /* inter-character timer unused */
         newtp.c_cc[VMIN]     = 0;     /* blocking read until 1 character arrives */
         newtp.c_cc[VSWTC]    = 0;     /* '\0' */
         newtp.c_cc[VSTART]   = 0;     /* Ctrl-q */
         newtp.c_cc[VSTOP]    = 0;     /* Ctrl-s */
         newtp.c_cc[VSUSP]    = 0;     /* Ctrl-z */
         newtp.c_cc[VEOL]     = 0;     /* '\0' */
         newtp.c_cc[VREPRINT] = 0;     /* Ctrl-r */
         newtp.c_cc[VDISCARD] = 0;     /* Ctrl-u */
         newtp.c_cc[VWERASE]  = 0;     /* Ctrl-w */
         newtp.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
         newtp.c_cc[VEOL2]    = 0;     /* '\0' */
   	                                                                                                                                             
	  tcflush(fd, TCIFLUSH);
	 tcsetattr(fd,TCSANOW,&newtp);

}

void  readport(void)
{
	unsigned char buff[20];
	
while (1) { 
	printf("reading\n");
  n = read(fd, &buff, 20);
//	fcntl(fd,F_SETFL,FNDELAY);
  if (n == -1) switch(errno) {
         case EAGAIN: /* sleep() */ 
         printf("eagain\n");
            continue;
          
         default: goto quit;
         }
  if (n ==0) break;
  buff[n]=0;

  printf("%d %s\n", n,buff);
  }
quit:
   printf("QUIT\n");
}

void my_message_callback(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *message)
{
	if(message->payloadlen){
		printf("%s %s\n", message->topic, message->payload);
		if (fd != 0){
			int n = write(fd, message->payload, message->payloadlen);
			if (n < 0)
                {
                        fputs("write() of bytes failed!\n", stderr);
                }
                else
                {
                        printf("Message sent successfully %d\n",n);
                        write(fd, "\n", 1);
                }
		}
	}else{
		printf("%s (null)\n", message->topic);
	}
	fflush(stdout);
}

void my_message_callback_internal(struct mosquitto *mosq, void *userdata, const struct mosquitto_message *message)
{
	if(message->payloadlen){
		printf("%s %s\n", message->topic, message->payload);
		
	}else{
		printf("%s (null)\n", message->topic);
	}
	fflush(stdout);
}

void my_connect_callback(struct mosquitto *mosq, void *userdata, int result)
{
	int i;
	if(!result){
		/* Subscribe to broker information topics on successful connect. */
		mosquitto_subscribe(mosq, NULL, "topic/test/1", 0);
	}else{
		fprintf(stderr, "Connect failed\n");
	}
}

void my_subscribe_callback(struct mosquitto *mosq, void *userdata, int mid, int qos_count, const int *granted_qos)
{
	int i;

	printf("Subscribed (mid: %d): %d", mid, granted_qos[0]);
	for(i=1; i<qos_count; i++){
		printf(", %d", granted_qos[i]);
	}
	printf("\n");
}

int main_local_config()
{
	printf("in another process\n");
	struct mosquitto *mosq = NULL;
	
	printf("main_outside_world\n");
	
	mosquitto_lib_init();
	mosq = mosquitto_new("test-openwrt-local", true, NULL);
	
	if(!mosq){
		fprintf(stderr, "Error: Out of memory.\n");
		return 1;
	}
	//mosquitto_connect_callback_set(mosq, my_connect_callback);
	mosquitto_message_callback_set(mosq, my_message_callback_internal);
	//mosquitto_subscribe_callback_set(mosq, my_subscribe_callback);
	printf("connecting2\n");
	if(mosquitto_connect(mosq, "127.0.0.1", 1883, 60)){
		fprintf(stderr, "Unable to connect.\n");
		return 1;
	}
	printf("connected2!\n");	
	mosquitto_subscribe(mosq, NULL, "topic/wifi_user", 0);
	
	int res;
	while(!(res=mosquitto_loop(mosq, -1, 100))){
		//printf("in loop\n");
	}
	
	return 0;
}

void on_disconnect_callback(struct mosquitto *mosq, void *obj, int rc)
{
	printf("Client disconnected from public\n");
	while (mosquitto_reconnect(mosq) != MOSQ_ERR_SUCCESS)
	{
		sleep(10);
		printf("Tring to reconnect\n");
		res_init();
	}
}

int main_outside_world()
{
	struct mosquitto *mosq = NULL;
	
	printf("main_outside_world\n");
	
	mosquitto_lib_init();
	openport();
	mosq = mosquitto_new("test-openwrt", true, NULL);
	
	if(!mosq){
		fprintf(stderr, "Error: Out of memory.\n");
		return 1;
	}
	mosquitto_connect_callback_set(mosq, my_connect_callback);
	mosquitto_disconnect_callback_set(mosq, on_disconnect_callback);
	mosquitto_message_callback_set(mosq, my_message_callback);
	//mosquitto_subscribe_callback_set(mosq, my_subscribe_callback);
	printf("connecting\n");
	while(mosquitto_connect(mosq, "iot.eclipse.org", 1883, 60)){
		fprintf(stderr, "Unable to connect.\n");
		sleep(10);
		res_init();
		//return 1;
	}
	printf("connected!\n");	
	mosquitto_subscribe(mosq, NULL, "topic/dimm_val", 0);
	
	int res;
	while(!(res=mosquitto_loop(mosq, -1, 100))){
		printf("in loop\n");
	}
	
        printf("Hello World %d\n", res);
        //openport();
        //readport();
        return 0;
}

int main(void)
{
	
	
	pid_t childPID;
    int var_lcl = 0;

    childPID = fork();
    
    if(childPID >= 0) // fork was successful
    {
        if(childPID == 0) // child process
        {
            main_local_config();
        }
        else
		{
			main_outside_world();
		}
	}
	else
	{
		printf("FORK ERROR!!!\n");
	}
	
	return 0;
	
} 
