#ifndef SAFE_QUEUE
#define SAFE_QUEUE

#include <queue>
#include <mutex>
#include <condition_variable>
#include <iostream>

template <class T>
class SafeQueue
{
public:
  SafeQueue(void)
    : q()
    , m()
    , c()
  {}

  ~SafeQueue(void)
  {}

  void enqueue(T&& t)
  {
	 // std::cout<<"ENQUEU!!!\n";
    std::lock_guard<std::mutex> lock(m);
    q.push(std::forward<T>(t));
    c.notify_one();
  }

  void enqueue(const T& t)
  {
    std::lock_guard<std::mutex> lock(m);
    q.push(t);
    c.notify_one();
  }

  T dequeue_block(void)
  {
    std::unique_lock<std::mutex> lock(m);
    while(q.empty())
    {
      c.wait(lock);
    }
 //   std::cout<<"dequeue_block\n";
//    if(std::is_move_constructible<T>::value)
//    {

        T val(std::move(q.front()));
        q.pop();
        return val;
    //}
//    else
//    {
//        std::cout<<"NOT is_move_constructible";
//        T val = q.front();
//        q.pop();
//        return val;
//    }

  }

  bool is_empty()
  {
      std::lock_guard<std::mutex> lock(m);
      return q.empty();
  }

private:
  std::queue<T> q;
  mutable std::mutex m;
  std::condition_variable c;
};
#endif
