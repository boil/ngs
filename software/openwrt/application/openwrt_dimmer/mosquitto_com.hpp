#ifndef MOSQUITTO_COM_HPP
#define MOSQUITTO_COM_HPP
#include <mosquitto.h>
#include <iostream>
#include "safe_queu.hpp"
#include "communication.hpp"
#include "basicmessage.hpp"
#include <string>

#ifdef FOR_ARM
void on_disconnect_wrapper(struct mosquitto *mosqu,void *obj,int rc);
void on_message_wrapper(struct mosquitto *mosqu, void *obj, const struct mosquitto_message *message);
void on_connect_wrapper(struct mosquitto * mosqu, void *obj, int rc);
#else
void on_disconnect_wrapper(void *obj);
void on_connect_wrapper(void *obj, int rc);
void on_message_wrapper(void *obj, const struct mosquitto_message *message);
#endif

class MosquittoConnection : public CommunicationGeneral
{


    BasicMessage connection_loop();

    int connect_resource();

    int disconnect_resource();

    int write_resource(BasicMessage &msg);
public:
    MosquittoConnection(std::string id)
{
    	auto_retry_connect = true;
    	mosq=nullptr;
    	connection_id = id;
}
    void on_disconnect();
    void on_connect(int rc);
    void on_message(const struct mosquitto_message *message);
    int  add_topic(std::string topic_name);

private:
    struct mosquitto *mosq;
    SafeQueue<BasicMessage> mosq_buff;
    std::vector<std::string> topics;
    std::string connection_id;
};
#endif // MOSQUITTO_COM_HPP
