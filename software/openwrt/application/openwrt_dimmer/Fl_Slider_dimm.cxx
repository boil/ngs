#include "Fl_Slider_dimm.h"
#include <iostream>
#include <math.h>
#include <FL/fl_draw.H>
#include <FL/Enumerations.H>

Fl_Slider_dimm::Fl_Slider_dimm(int x,int y,int w,int h):Fl_Slider(x,y,w,h)
{
	for(int i=0;i<40;i++)
  {
	for (int j=0;j<40;j++)
	{
		int temp=pow((i-20),2)+pow((j-20),2);
		//if(temp<=(15^2)) disk[i+j*40]=0xff;
		//else if (temp<=(20^2)) disk[i+j*40]=0x50;
		//else disk[i+j*40]=0x00;
		
		if(temp<=pow(10,2))
		{
		disk[i*40+j]=0xff;
		//std::cout<<"0xff ";
		}else if(temp<=pow(20,2))
		disk[i*40+j]=0x80;
		//std::cout<<"0x00 ";
	}
	//std::cout<<"\n";
  }
  
  Fl_Color c = Fl::get_color(180);
  buf = (uchar*)malloc(40*40*4);
  uchar r = (c >> 24) & 0xff;
            uchar g = (c >> 16) & 0xff;
            uchar b = (c >> 8 ) & 0xff;
  uchar *out = buf;
            // Fill the buffer:
            //     Insert the rgb values, and copy just the alpha frmo the disk[] buffer.
            //
            for ( int t=0; t<40*40; t++ )
                { *out++ = r; *out++ = g; *out++ = b; *out++ = disk[t]; }
}

void Fl_Slider_dimm::draw_bg(int X, int Y, int W, int H)
{
	fl_push_no_clip();
  fl_color((Fl_Color)55);
  fl_rectf(X, Y-20, W+20, H+40);
  fl_color(FL_FOREGROUND_COLOR);
  fl_rect(X+W/2, Y, 2, H);
  fl_color(((Fl_Color)180));
  fl_rectf(X+W/2-1, Y+(double)H*value(), 6, (double)H*(1-value()));
  fl_pop_clip();
}

void Fl_Slider_dimm::draw()
{
	draw(x()+Fl::box_dx(box()),
       y()+Fl::box_dy(box()),
       w()-Fl::box_dw(box()),
       h()-Fl::box_dh(box()));
       
	
}

void Fl_Slider_dimm::draw(int X, int Y, int W, int H)
{
	draw_bg(X, Y, W, H);
  fl_push_no_clip();
  Fl_RGB_Image *img = new Fl_RGB_Image(buf,40,40,4);
  img->draw(X+W/2-18, Y+(double)H*value()-10);
  delete img;
  fl_pop_clip();
}
