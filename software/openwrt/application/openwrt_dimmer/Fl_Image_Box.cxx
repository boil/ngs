#include "Fl_Image_Box.h"
#include <iostream>

extern unsigned char wifi_icon_png[];
Fl_Image_Box::Fl_Image_Box(int x,int y,int w,int h):Fl_Box(x,y,w,h)
{
	img_on = new Fl_RGB_Image(wifi_icon_png,30,30,4);
	wifi_state=WIFI_OFF_STATE;
	change_wifi_icon();
	//Fl_Button::clear_visible_focus();
}

void Fl_Image_Box::change_wifi_icon()
{
	for(int i=3;i<30*30*4;i+=4)
	{
		if(wifi_icon_png[i]!=0)
		{
			if(wifi_state==WIFI_ON_STATE)
			{
				wifi_icon_png[i-3]=0x3C;
				wifi_icon_png[i-2]=0xb3;
				wifi_icon_png[i-1]=0x71;
			}
			else
			{
				wifi_icon_png[i-3]=0xDC;
				wifi_icon_png[i-2]=0x14;
				wifi_icon_png[i-1]=0x3C;
			}
		}
	}
}

void Fl_Image_Box::change_wifi_state(char wifi_state_param)
{
	wifi_state = wifi_state_param;
	change_wifi_icon();
}

void Fl_Image_Box::draw()
{
	img_on->draw(x(),y());
}
