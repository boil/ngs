#include "mosquitto_com.hpp"
#include <string.h>
#include <stdlib.h>

#ifdef FOR_ARM
void on_disconnect_wrapper(struct mosquitto *mosqu,void *obj,int rc)
#else
void on_disconnect_wrapper(void *obj)
#endif
{
    MosquittoConnection *m = (MosquittoConnection *)obj;
    m->on_disconnect();
}

#ifdef FOR_ARM
void on_message_wrapper(struct mosquitto *mosqu, void *obj, const struct mosquitto_message *message)
#else
void on_message_wrapper(void *obj, const struct mosquitto_message *message)
#endif
{
    MosquittoConnection *m = (MosquittoConnection *)obj;
    m->on_message(message);
}

#ifdef FOR_ARM
void on_connect_wrapper(struct mosquitto * mosqu, void *obj, int rc)
#else
void on_connect_wrapper(void *obj, int rc)
#endif
{
    MosquittoConnection *m = (MosquittoConnection *)obj;
    m->on_connect(rc);
}

BasicMessage MosquittoConnection::connection_loop()
{
    BasicMessage tmp_msg;
#ifdef FOR_ARM
    mosquitto_loop(mosq, 0, 1);
#else
    mosquitto_loop(mosq, 0);
#endif
    if(!mosq_buff.is_empty())
    {
        //std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
        tmp_msg=mosq_buff.dequeue_block();
        //std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1\n";
    }
    return tmp_msg;
}

int MosquittoConnection::write_resource(BasicMessage &msg)
{
	std::string s=msg.info_str["topic"];
	if(s=="")
		return 1;
	mosquitto_publish(mosq, NULL, s.c_str(), msg.get_size(), (uint8_t*)msg.get_data_point(), 0, false);
	//std::cout<<"In write_resource\n";
	return 0;
}

int MosquittoConnection::connect_resource()
{
    auto search_address = connection_optons.find("address");
    auto search_port = connection_optons.find("port");
    if(search_address == connection_optons.end())
    {
        std::cout<<"No address for mosquitto\n";
        return 1;
    }
    if(search_port == connection_optons.end())
    {
        std::cout<<"No port for mosquitto\n";
        return 1;
    }
    std::cout<<"Address: "<< search_address->second << "\nPort: "<<search_port->second<<"\n";
#ifdef FOR_ARM
    mosq =  mosquitto_new(connection_id.c_str(), true, this);
#else
    mosq =  mosquitto_new(connection_id.c_str(), this);
#endif
    if(!mosq){
            std::cout<< "Error: Out of memory.\n";
            return 1;
    }
    else
        std::cout<<"Connecting\n";
    mosquitto_connect_callback_set(mosq, on_connect_wrapper);
    mosquitto_disconnect_callback_set(mosq, on_disconnect_wrapper);
    mosquitto_message_callback_set(mosq, on_message_wrapper);

#ifdef FOR_ARM
    if(mosquitto_connect(mosq, search_address->second.c_str(), atoi(search_port->second.c_str()), 60)!=MOSQ_ERR_SUCCESS)
#else
    if(mosquitto_connect(mosq, search_address->second.c_str(), atoi(search_port->second.c_str()), 60,true)!=MOSQ_ERR_SUCCESS)
#endif
    {
        std::cout<<"Cann't connect\n";
        return 1;
    }
    else std::cout<<"Conencted!\n";

    return 0;
}

int MosquittoConnection::disconnect_resource()
{
    std::cout<<"disconnect_resource\n";
    mosquitto_disconnect(mosq);
    return 0;
}

void MosquittoConnection::on_connect(int rc)
{
    if(!rc)
    {
        std::cout << "on_connect " << std::this_thread::get_id() << "\n";
        //mosquitto_subscribe(mosq, NULL, "topic/test/1", 0);
        for(auto topic:topics)
        	mosquitto_subscribe(mosq, NULL, topic.c_str(), 0);
        unpause_connection();
        connection_established = true;
    }
    else
        std::cout<<"Connected failed\n";
}

void MosquittoConnection::on_disconnect()
{
    std::cout<<"on_disconnect\n";
    std::cout << "on_disconnect " << std::this_thread::get_id() << "\n";
    pause_connection();
    connection_established = false;
    while(run_loop)
    {
        std::chrono::milliseconds dura( 5000 );


        std::this_thread::sleep_for( dura );
        if(mosquitto_reconnect(mosq)==MOSQ_ERR_SUCCESS)
        {
            unpause_connection();
            std::cout<<"reconnected\n";
            break;
        }
    }
}

void MosquittoConnection::on_message(const struct mosquitto_message *message){
    if(message->payloadlen)
    {
        //std::cout << "on_message " << std::this_thread::get_id() << "\n";
          //  std::cout<< message->topic<<" " << message->payload<<" "<<strlen((char*)message->payload)<<"\n";
            std::string tmp_str;

            BasicMessage tmp_msg(std::map<std::string, std::string> ({{"type", "mosquitto"}, {"topic", message->topic}}));
            tmp_msg.put_data((char*)message->payload, message->payloadlen+1);
            //std::cout<<"DEBGU: "<<tmp_msg.get_data_point()<<"\n";
            mosq_buff.enqueue(std::move(tmp_msg));
            //std::cout<<"DEBGU: "<<tmp_msg.get_data_point()<<"\n";
    }
}

int  MosquittoConnection::add_topic(std::string topic_name)
{
	topics.push_back(topic_name);
	if(connection_established)
		mosquitto_subscribe(mosq, NULL, topic_name.c_str(), 0);
	return 0;
}
