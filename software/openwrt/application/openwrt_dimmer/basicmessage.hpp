#ifndef BASICMESSAGE_HPP
#define BASICMESSAGE_HPP
#include <cstring>
#include <iostream>
#include<map>
#include <functional>
#include <chrono>
#include <future>

struct AVRMessage
{
	unsigned char changed_field;
	unsigned char light_intensity;
};

inline void SwapEndian(unsigned short int &val)
{
	val = (val<<8) | (val>>8);
}

class BasicMessage
{
public:
    BasicMessage(std::map<std::string, std::string> info){
        point=nullptr;size_local=0;info_str=info;}
    BasicMessage(){point=nullptr;size_local=0;}
    void add_info(std::map<std::string, std::string> info){info_str=info;}
    bool put_data(const char* data,int size)
    {
        if(point!=nullptr)
            return false;
        size_local=size;
        point = new char[size];
        std::memcpy(point, data, size);
        return true;
    }
    void remove_data()
    {
        info_str.clear();
        if(point!=nullptr)
        {
            delete [] point;
            point =nullptr;
            size_local=0;
        }
    }

    ~BasicMessage()
    {
        remove_data();
    }

    BasicMessage(const BasicMessage& other)
    {
        point=nullptr;
        size_local=0;
        //remove_data();
        //std::cout<<"IN copy CSTR2: "<<other.point<<" "<<other.size_local<<"\n";
        info_str=other.info_str;
        put_data(other.point, other.size_local+1);
        //std::cout<<"IN copy CSTR3: "<<point<<" "<<size_local<< "\n";
    }

    BasicMessage(BasicMessage&& other)
    {
        //std::cout<<"IN MOVE CSTR\n";
        point=nullptr;
        size_local=0;
        //remove_data();
        //std::cout<<"IN MOVE CSTR2\n";

        if(other.point)
        {
            //std::cout<<"IN MOVE CSTR2: "<<other.point<<"\n";
            point = other.point;
            other.point=nullptr;
            size_local = other.size_local;
        }
        info_str=other.info_str;
        //std::cout<<"IN MOVE CSTR3: "<<point<<"\n";
    }

    int get_size()
    {
        return size_local;
    }

    char* get_data_point()
    {
        return point;
    }

    char * get_data_copy()
    {
        char *tmp_point = nullptr;
        if (size_local>0)
        {
            tmp_point = new char[size_local];
            std::memcpy(tmp_point, point, size_local);
        }
        return tmp_point;
    }

    BasicMessage& operator=(const BasicMessage& other)
    {
        //std::cout<<"IN copy =\n";
            remove_data();
            info_str=other.info_str;
            put_data(other.point, other.size_local);

        return *this;
    }

    BasicMessage& operator=(BasicMessage&& other)
    {

        remove_data();
        if(other.point)
        {
            point = other.point;
            other.point=nullptr;
            size_local = other.size_local;
        }
        info_str = other.info_str;
        //SWif(point)
        //std::cout<<"IN MOVE = "<<point<<"\n";
        //std::cout<<"OUT MOVE =\n";
        return *this;
    }
    std::map<std::string, std::string> info_str;
private:

    char *point;
    int size_local;
};

class later
{
public:
    template <class callable, class... arguments>

    void start(int after, bool async, int max_count, callable&& f, arguments&&... args)
    {
		counter=0;
		max_counter=max_count;
        std::function<typename std::result_of<callable(arguments...)>::type()> task(std::bind(std::forward<callable>(f), std::forward<arguments>(args)...));

        if (async)
        {

            std::thread([after, task, this]() {
				while(counter<max_counter)
				{
					std::this_thread::sleep_for(std::chrono::milliseconds(after));
					if(counter<max_counter)
						task();
					//after_task2();
					counter++;
				}
				}).detach();

        }
        else
        {
			while(counter<max_counter)
				{
            std::this_thread::sleep_for(std::chrono::milliseconds(after));
            if(counter<max_counter)
            	task();
            //after_task2();
            counter++;
		}
        }
    }



	private:
	int counter,max_counter, milisecs;
	public:
	bool isrunnig()
	{
		return (counter<max_counter);
	}

	void stop()
	{
		counter=max_counter;

	}

	bool last_execution()
	{
		return (max_counter-counter ==1);
	}

};

#endif // BASICMESSAGE_HPP
