#ifndef GENERAL_COMM
#define GENERAL_COMM

#include <thread>
#include "safe_queu.hpp"
#include <iostream>
#include <chrono>
#include <string>
#include <map>
#include <vector>
#include "basicmessage.hpp"
#include <mutex>
#include <condition_variable>

enum Error_ID {
	NOERROR = 0,
	mosq_failed_connect=100,
	general_connect_failed,
	serial_failed_connect,
	mosq_failed_recv,
	mosq_failed_send,
	serial_failed_recv,
	serial_failed_send
};

class CommunicationGeneral
{
public:
    CommunicationGeneral(std::map<std::string, std::string> option): connection_optons(option){run_loop =false;pauseconnection=false;connection_established=false;}
    CommunicationGeneral(){run_loop=false;pauseconnection=false;connection_established=false;}
    void init(std::map<std::string, std::string>&& option);
    Error_ID connect();
    bool disconnect();
    bool send_message(BasicMessage &msg);
    bool pause_connection();
    bool unpause_connection();
    void subscribe_queu(SafeQueue<BasicMessage>* sub_queu)
    {
        subscribed_queus.push_back(sub_queu);
    }

protected:
    std::vector<SafeQueue<BasicMessage>*> subscribed_queus;
    std::map<std::string, std::string> connection_optons;
    std::thread connection_thred;
    SafeQueue<BasicMessage> commads_queu;
    virtual BasicMessage connection_loop() = 0;
    virtual int connect_resource() = 0;
    virtual int disconnect_resource() = 0;
    virtual int write_resource(BasicMessage &msg) = 0;

    bool run_loop;
    bool pauseconnection;
    bool auto_retry_connect;
    bool connection_established;
private:
    void connection_thred_function();


};

#endif
