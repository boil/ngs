#include "Fl_Button_Dimm.h"
#include <iostream>

extern unsigned char onoff_pic[];
Fl_Button_Dimm::Fl_Button_Dimm(int x,int y,int w,int h,const char* e):Fl_Button(x,y,w,h)
{
	Fl_Button::clear_visible_focus();
}
Fl_Button_Dimm::Fl_Button_Dimm(int x,int y,int w,int h):Fl_Button(x,y,w,h)
{
	img_on = new Fl_RGB_Image(onoff_pic,50,50,4);
	button_state=BUTTON_ON_STATE;
	Fl_Button::clear_visible_focus();
}

void Fl_Button_Dimm::change_button_state()
{
	button_state=!button_state;
	
	for(int i=3;i<50*50*4;i+=4)
	{
		if(onoff_pic[i]==0xff)
		{
			if(button_state==BUTTON_ON_STATE)
			{
				onoff_pic[i-3]=0x3C;
				onoff_pic[i-2]=0xb3;
				onoff_pic[i-1]=0x71;
			}
			else
			{
				onoff_pic[i-3]=0xDC;
				onoff_pic[i-2]=0x14;
				onoff_pic[i-1]=0x3C;
			}
		}
	}
}

int Fl_Button_Dimm::handle(int event)
{
	//int retval=Fl_Button::handle(event);
	if(event==FL_RELEASE)
	{
		change_button_state();
		
		do_callback();
		redraw();
	}
	return 1;
}

void Fl_Button_Dimm::draw()
{
	std::cout<<"button_state: "<<(int)button_state<<"\n";
	img_on->draw(x(),y());
}
