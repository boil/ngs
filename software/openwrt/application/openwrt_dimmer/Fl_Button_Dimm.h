#ifndef __FL_BUTTON_DIMM_H
#define __FL_BUTTON_DIMM_H
#include <FL/Fl_Button.H>
#include <FL/Fl.H>
#include <FL/Fl_Image.H>

#define BUTTON_ON_STATE 1
#define BUTTON_OFF_STATE 0

class Fl_Button_Dimm: public Fl_Button {
private:
	Fl_RGB_Image *img_on;
	char button_state;
	void change_button_state();
public:
	void draw();
	
	int handle(int event);
	Fl_Button_Dimm(int x,int y,int w,int h,const char* e);
	Fl_Button_Dimm(int x,int y,int w,int h);
};

#endif
