#ifndef SERIAL_COM_HPP
#define SERIAL_COM_HPP
#include "basicmessage.hpp"
#include "communication.hpp"

class SerialConnection : public CommunicationGeneral
{
public:
	SerialConnection():bytes_to_read(0)
	{
		auto_retry_connect = false;
		serial_descriptor=0;
	}
private:
    BasicMessage connection_loop();

    int connect_resource();

    int disconnect_resource();

    int write_resource(BasicMessage &msg);

    int serial_descriptor;
    unsigned short int bytes_to_read;
};
#endif // SERIAL_COM_HPP
