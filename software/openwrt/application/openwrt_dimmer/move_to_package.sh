package_path="/home/boil/Documents/opewrt/tmp/openwrt/package/utils/openwrt_dimmer/src"
rm -rf $package_path/*
cp -v *.cpp $package_path
cp -v *.hpp $package_path
cp -v *.cxx $package_path
cp -v *.h $package_path
cp -v package_makefile $package_path/Makefile
cp -v dimm_startup $package_path/dimm_startup
cp -vr etc $package_path
