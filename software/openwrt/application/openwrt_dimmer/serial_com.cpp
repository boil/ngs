#include "serial_com.hpp"
#include "touchscreen.hpp"
#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <cstring>

int SerialConnection::connect_resource()
{
    auto search_device = connection_optons.find("device");
    auto search_speed = connection_optons.find("speed");
    struct termios oldtp, newtp;

    if(search_device == connection_optons.end())
    {
        std::cout<<"No device for serial connection\n";
        return 1;
    }
    if(search_speed == connection_optons.end())
    {
        std::cout<<"No connection speed \n";
        return 1;
    }

    std::cout<<"device: "<< search_device->second << "\nSpeed: "<<search_speed->second<<"\n";
    serial_descriptor = open(search_device->second.c_str(), O_RDWR | O_NOCTTY |O_NDELAY );
    if (serial_descriptor==-1)
    {
        std::cout<<"Error openning serial port\n";
        return 1;
    }

    fcntl(serial_descriptor,F_SETFL,FNDELAY);

    tcgetattr(serial_descriptor,&oldtp); /* save current serial port settings */
    //     tcgetattr(serial_descriptor,&newtp); /* save current serial port settings */
    bzero(&newtp, sizeof(newtp));
    //       bzero(&oldtp, sizeof(oldtp));
    newtp.c_cflag &= ~PARENB;
    newtp.c_cflag &= ~CSTOPB;
    newtp.c_cflag &= ~CSIZE;
    newtp.c_cflag &= ~CRTSCTS;

    newtp.c_cflag = atoi(search_speed->second.c_str()) | CS8 | CLOCAL | CREAD;

    newtp.c_iflag = IGNPAR | ICRNL;
    newtp.c_iflag &= ~(IXON | IXOFF | IXANY);

    newtp.c_oflag = 0;
    newtp.c_lflag = 0;//(ICANON | ECHO | ECHOE);
    newtp.c_cc[VINTR]    = 0;     /* Ctrl-c */
    newtp.c_cc[VQUIT]    = 0;     /* Ctrl-\ */
    newtp.c_cc[VERASE]   = 0;     /* del */
    newtp.c_cc[VKILL]    = 0;     /* @ */
    // newtp.c_cc[VEOF]     = 4;     /* Ctrl-d */
    newtp.c_cc[VTIME]    = 0;     /* inter-character timer unused */
    newtp.c_cc[VMIN]     = 1;     /* blocking read until 1 character arrives */
    newtp.c_cc[VSWTC]    = 0;     /* '\0' */
    newtp.c_cc[VSTART]   = 0;     /* Ctrl-q */
    newtp.c_cc[VSTOP]    = 0;     /* Ctrl-s */
    newtp.c_cc[VSUSP]    = 0;     /* Ctrl-z */
    newtp.c_cc[VEOL]     = 0;     /* '\0' */
    newtp.c_cc[VREPRINT] = 0;     /* Ctrl-r */
    newtp.c_cc[VDISCARD] = 0;     /* Ctrl-u */
    newtp.c_cc[VWERASE]  = 0;     /* Ctrl-w */
    newtp.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
    newtp.c_cc[VEOL2]    = 0;     /* '\0' */

    tcflush(serial_descriptor, TCIFLUSH);
    tcsetattr(serial_descriptor,TCSANOW,&newtp);
    return 0;
}

int SerialConnection::disconnect_resource()
{
    std::cout<<"disconnect_resource\n";
    close(serial_descriptor);
    return 0;
}

int SerialConnection::write_resource(BasicMessage &msg)
{
	serial_signature sig;
	int result;

	fcntl(serial_descriptor,F_SETFL,0);

	std::string s = msg.info_str["type"];
	std::cout<<"In serial_comm send: "<<msg.get_data_point()<<"\n";

	if(s!="raw")
	{
		sig.package_size = msg.get_size();
		sig.signiture = serial_signiture_byte;
		result = write(serial_descriptor, (char*)&sig, sizeof(sig));
		if(result == -1)
		{
			std::cout<<"Error in serial write !!!\n";
			return 1;
		}

		result = write(serial_descriptor, msg.get_data_point(), sig.package_size);
		if(result == -1)
		{
			std::cout<<"Error in serial write !!!\n";
			return 1;
		}


	}
	else
	{
		result = write(serial_descriptor, msg.get_data_point(), msg.get_size());
		char x='\n';
		if (-1 == write(serial_descriptor,&x,1))
			std::cout<<"ERROR!!! Could not write to serial!!!\n";
	}
	fcntl(serial_descriptor,F_SETFL,FNDELAY);
	return 0;
}

BasicMessage SerialConnection::connection_loop()
{
    unsigned int bytes;
    static serial_signature sig;

    BasicMessage tmp_msg;
    int res = ioctl(serial_descriptor, FIONREAD, &bytes);
    if (res==-1)
    {
    	std::cout<<"Error ioctl in serial connection loop\n";
    	return tmp_msg;
    }
    //read(serial_descriptor, buffer, bytes);

    //std::cout<<"result: "<< res<<" "<<bytes<<" "<<buffer<<"\n";
    if(bytes!=0)
    {
        //std::cout<<"bytes available on serial: "<<bytes<<"\n";
        if((bytes_to_read == 0)&&(bytes>=sizeof(serial_signature)))
        {

            int bytesread = read(serial_descriptor, &sig, sizeof(serial_signature));
            //std::cout<<"read1 : "<<bytesread<<"\n";
            if(bytesread == -1)
                std::cout<<"serial read returned -1\n";
            else if (sig.signiture != serial_signiture_byte)
            {
                std::cout<<"wrong signiture in serial read "<<sig.signiture<<"\n";
                int res = ioctl(serial_descriptor, FIONREAD, &bytes);
                if(res==-1)
                {
					std::cout<<"ERROR!!! Could not ioctl the read bytes\n";
					return tmp_msg;
				}
                char *tmp_buf = new char[bytes];
                int bytesread = read(serial_descriptor, tmp_buf, bytes);
                if(bytesread==-1)
					std::cout<<"ERROR on trying to emty serial buffer!!!\n";
                delete [] tmp_buf;
            }
            else
            {
            	//std::cout<<"Got sig: "<<sig.package_size<<" "<<sig.pad<<" "<< sizeof(serial_signature)<<"\n";
            	bytes_to_read = sig.package_size;
#ifdef FOR_ARM
            	SwapEndian(bytes_to_read);
#endif
            	sig.package_size = bytes_to_read;
            	//if(sig.pad=='D')
            		//std::cout<<"Got sig: "<<sig.package_size<<" "<<bytes_to_read<<" "<<sig.pad<<" "<< sizeof(serial_signature)<<"\n";
            }
            //std::cout<<"bytes available on serial: "<<bytes<<"\n";
        }else if (bytes>=bytes_to_read)
        {
            char *buff = new char[bytes_to_read];
            int bytesread = read(serial_descriptor, buff, bytes_to_read);
            //std::cout<<"read2 : "<<bytesread<<"\n";
            if (bytesread==-1)
                std::cout<<"serial read returned -1 2\n";
            tmp_msg.put_data(buff, bytes_to_read);
            delete [] buff;
            tmp_msg.add_info(std::map<std::string, std::string> ({{"type", "serial"}}));
            if(sig.pad=='T')
            {
				//printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
            	tmp_msg.add_info(std::map<std::string, std::string> ({{"function", "touch"},{"type", "serial"}}));
            }else
            if(sig.pad=='D')
            {
            	//std::cout<<"GOT SERIAL D!!!!! "<<bytes<<" "<<bytesread<<" "<<bytes_to_read<<"\n";
            	tmp_msg.add_info(std::map<std::string, std::string> ({{"function", "dimmer"},{"type", "serial"}}));
            }
            bytes_to_read = 0;
        }
    }

    return tmp_msg;
}
