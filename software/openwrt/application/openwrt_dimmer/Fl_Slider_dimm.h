#ifndef __FL_SLIDER_DIMM_H
#define __FL_SLIDER_DIMM_H
#include <FL/Fl_Slider.H>
#include <FL/Fl.H>
#include <FL/Fl_Image.H>

class Fl_Slider_dimm: public Fl_Slider {
private:
	unsigned char disk[40*40];
	uchar *buf;
public:
	void draw();
	void draw(int X, int Y, int W, int H);
	void draw_bg(int X, int Y, int W, int H);
	Fl_Slider_dimm(int x,int y,int w,int h);
};

#endif
