//touch screen stuff - temporary move        
            		unsigned short int *coords;
            		int pressure;
            		coords= (unsigned short int *)tmp_msg.get_data_point();
            		//SwapEndian(coords[0]);
            		//SwapEndian(coords[1]);
            		//SwapEndian(coords[2]);

            		touchscreen_point temp_point;
            		temp_point.X = coords[0];
            		temp_point.Y = coords[1];
            		temp_point.zero_counter = coords[2];
            		if(coords[3]>0)
            			//temp_point.pressure = 600*temp_point.X/1024*(1024/coords[3] -1)- 320*(1-temp_point.Y/1024);
            			pressure= 600*(double)temp_point.X/1024*((double)1024/coords[3] -1)- 320*(1-(double)temp_point.Y/1024);
            		else
            			//temp_point.pressure =0;
            			pressure=0;

            		if ((temp_point.X!=0)&&(pressure>-10))
            			continue;
            		//check if there is a finger on the screen
            		if(temp_point.X>0)
            		{
            			touch_points.wait_for_touch=false;
            			//measure the distanced traveled
            			if(touch_points.number_of_points>0)
            			{
            				touch_points.gesture_distance += temp_point.X - touch_points.last_point.X;
            				int temp_diff=abs(touch_points.gesture_distance)-abs(temp_distance);
            				//check if we need to modify the light intensity
            				if(temp_diff>=touch_increase_distance)
            				{

            					int increasewith= temp_diff/touch_increase_distance;
            					if(temp_diff%touch_increase_distance > touch_increase_distance/2)
            						increasewith++;
            					std::cout<<"temp_diff: "<<temp_diff<<" increasewith:"<<increasewith<<"\n";
            					if(touch_points.gesture_distance>0)
            						dimm_level+=10*increasewith;
            					else
            						dimm_level-=10*increasewith;
            					if(dimm_level>120)
            						dimm_level=120;
            					if(dimm_level<0)
            						dimm_level=0;
            					dim_level_changed = true;
            					temp_distance = touch_points.gesture_distance;
            				}
            				//std::cout<<"new distance:"<<touch_points.gesture_distance<<"\n";
            			}
            			if(abs(touch_points.gesture_distance) > touch_tap_distance)
            			{
            				touch_points.wait_for_touch=true;
            				touch_points.number_of_taps=0;
            			}
            			std::memcpy(&touch_points.last_point, &temp_point, sizeof(touchscreen_point));
            			touch_points.number_of_points++;
            		}
            		else
            		{
            			touch_points.gesture_distance=0;
            			touch_points.number_of_points=0;
            			temp_distance=0;
            		}

            		//check if the finger was removed from screen
            		if((temp_point.zero_counter>0)&&(!touch_points.wait_for_touch))
            		{
            			if(abs(touch_points.gesture_distance) < touch_tap_distance)
            			{
            				std::cout<<"INCREASE TAPS\n";
            				touch_points.number_of_taps++;
            				touch_points.wait_for_touch=true;
            			}
            		}

            		//end of gesture
            		if(temp_point.zero_counter>2)
            		{
            			if(abs(touch_points.gesture_distance)< touch_tap_distance)
            			{
            				std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
            			if(touch_points.number_of_taps==1)
            			{
            				std::cout<<"SINGLE TAP!!!!\n";
            				if(dimm_level==0)
            				{
            					dimm_level = old_dimm_level;
            					dim_level_changed = true;
            				}
            				else
            				{
            					old_dimm_level=dimm_level;
            					dimm_level=0;
            					dim_level_changed = true;
            				}
            			}
            			else if(touch_points.number_of_taps==2)
            			{
            				std::cout<<"DOUBLE TAP!!!\n";
            				dimm_level = 120;
            				dim_level_changed = true;
            			}
            			else if(touch_points.number_of_taps>2)
            				std::cout<<"TOO MANY TAPS!!!\n";
            			}
            			if(touch_points.wait_for_touch)
            			{
            				std::memset(&touch_points, 0, sizeof(touchscreen_gestures));
            				touch_points.wait_for_touch = true;
            			}
            			else
            				std::memset(&touch_points, 0, sizeof(touchscreen_gestures));
            		}

            		if(temp_point.zero_counter>9)
            		{
            			std::memset(&touch_points, 0, sizeof(touchscreen_gestures));
            			touch_points.wait_for_touch = true;
            			temp_distance=0;
            		}

            		if(dim_level_changed)
            		{
            			BasicMessage msg;
            			msg.add_info(std::map<std::string, std::string> ({{"type", "raw"}}));
            			std::ostringstream ss;
            			ss<<"dimmer:"<<120 - dimm_level;
            			std::cout<<ss.str()<<"\n";
            			msg.put_data(ss.str().c_str(), 1+strlen(ss.str().c_str()));
            			test_serial.send_message(msg);
            			dim_level_changed=false;
            		}

            		std::cout<<"X:"<< coords[0]<<" Y:"<<coords[1]<<" Count:"<<coords[2]<<" pressure:"<<coords[3]<<" "<<pressure<<" distance:"<<touch_points.gesture_distance<<" dimm_level:"<<dimm_level<< "\n";
            	} else if (funtion_str== "touch_reset")
            		std::memset(&touch_points, 0, sizeof(touchscreen_gestures));
