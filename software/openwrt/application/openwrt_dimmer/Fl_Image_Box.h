#ifndef __FL_IMAGE_BOX_H
#define __FL_IMAGE_BOX_H
#include <FL/Fl_Box.H>
#include <FL/Fl.H>
#include <FL/Fl_Image.H>

#define WIFI_ON_STATE 1
#define WIFI_OFF_STATE 0

class Fl_Image_Box: public Fl_Box {
private:
	Fl_RGB_Image *img_on;
	char wifi_state;
	void change_wifi_state(char wifi_state_param);
	void change_wifi_icon();
public:
	void draw();

	Fl_Image_Box(int x,int y,int w,int h);
};

#endif
