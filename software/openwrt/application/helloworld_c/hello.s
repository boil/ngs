	.file	1 "hello.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.gnu_attribute 4, 3
	.abicalls
	.option	pic0
	.rdata
	.align	2
$LC0:
	.ascii	"Hello World\000"
	.align	2
$LC1:
	.ascii	"GOT:%d\012\000"
	.text
	.align	2
	.globl	main
	.set	mips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$17,24,$31		# vars= 8, regs= 2/0, args= 16, gp= 8
	.mask	0x80020000,-4
	.fmask	0x00000000,0
	save	40,$17,$31
	addiu	$17,$sp,16
	lw	$4,$L2
	jal	puts
	li	$4,120
	jal	toupper
	sw	$2,8($17)
	lw	$2,8($17)
	move	$24,$2
	move	$5,$24
	lw	$4,$L3
	jal	printf
	move	$2,$24
	move	$sp,$17
	restore	24,$17,$31
	j	$31
	.align	2
$L2:
	.word	$LC0
$L3:
	.word	$LC1
	.end	main
	.size	main, .-main
	.ident	"GCC: (OpenWrt/Linaro GCC 4.9-2014.10 r44053) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
