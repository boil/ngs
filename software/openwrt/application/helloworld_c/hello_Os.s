	.file	1 "hello.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.gnu_attribute 4, 3
	.abicalls
	.option	pic0
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align	2
$LC0:
	.ascii	"Hello World\000"
	.align	2
$LC1:
	.ascii	"GOT:%d\012\000"
	.section	.text.startup,"ax",@progbits
	.align	2
	.globl	main
	.set	mips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,32,$31		# vars= 0, regs= 1/0, args= 16, gp= 8
	.mask	0x80000000,-4
	.fmask	0x00000000,0
	save	32,$31
	lw	$4,$L3
	jal	puts
	lw	$2,$L4
	lw	$4,$L5
	lw	$2,0($2)
	lh	$5,240($2)
	jal	printf
	restore	32,$31
	j	$31
	.align	2
$L3:
	.word	$LC0
$L4:
	.word	__ctype_toupper
$L5:
	.word	$LC1
	.end	main
	.size	main, .-main
	.ident	"GCC: (OpenWrt/Linaro GCC 4.9-2014.10 r44053) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
