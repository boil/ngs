package com.example.boil.myapplication3;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.util.Log;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import net.sf.xenqtt.client.MqttClient;
import net.sf.xenqtt.client.MqttClientListener;
import net.sf.xenqtt.client.PublishMessage;
import net.sf.xenqtt.client.Subscription;
import net.sf.xenqtt.client.SyncMqttClient;
import net.sf.xenqtt.message.ConnectReturnCode;
import net.sf.xenqtt.message.QoS;

import java.util.Random;


public class MyActivity extends Activity {

    private SeekBar seekBar;
    private Switch mySwitch;
    private MqttClientListener listener;
    private int progress = 0;
    private int last_progress = 0;
    SyncMqttClient client;

    private void send_value(int value)
    {
        int valuetosend;
        valuetosend = 120 - value;
        Log.i ("info", "value:" + valuetosend);
        try {
        client.publish(new PublishMessage("topic/test/1", QoS.AT_LEAST_ONCE, "dimmer:" + valuetosend));
        } catch (Exception ex) {
            Log.i("info","An exception prevented the publishing of the full catalog." + ex.toString());
        }
        finally {
            Log.i("info","Message SENT!!!");
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        listener = new MqttClientListener() {

            @Override
            public void publishReceived(MqttClient client, PublishMessage message) {
                //log.warn("Received a message when no subscriptions were active. Check your broker ;)");
            }

            @Override
            public void disconnected(MqttClient client, Throwable cause, boolean reconnecting) {
                if (cause != null) {
                    Log.i ("error", "Disconnected from the broker due to an exception." + cause.toString());
                    // log.error("Disconnected from the broker due to an exception.", cause);
                } else {
                    // log.info("Disconnected from the broker.");
                    Log.i ("error", "Disconnected from the broker due to an exception.");
                }

                if (reconnecting) {
                    Log.i ("error", "Attempting to reconnect to the broker.");
                    //log.info("Attempting to reconnect to the broker.");
                }
            }
        };

        client = new SyncMqttClient("tcp://test.mosquitto.org:1883", listener, 5);
        Random r = new Random();
        String client_id = "testdimmer" + r.nextInt();

        ConnectReturnCode returnCode = client.connect(client_id, true);
        if (returnCode != ConnectReturnCode.ACCEPTED) {
            //log.error("Unable to connect to the broker. Reason: " + returnCode);

            Log.i ("info", "could not coneect to server!!!." + client_id);
        }
        else
        {
            Log.i ("info", "Connection OK" + client_id);
        }

        seekBar = (SeekBar) findViewById(R.id.seekBar1);
        mySwitch = (Switch) findViewById(R.id.switch1);

        mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                    seekBar.setProgress(last_progress);
                    send_value(last_progress);
                    seekBar.setEnabled(true);
                }else{
                    last_progress = seekBar.getProgress();
                    seekBar.setProgress(0);
                    send_value(0);
                    seekBar.setEnabled(false);
                }

            }
        });


        seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {



            @Override

            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {

                progress = progresValue;



            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {



                send_value(progress);


            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
