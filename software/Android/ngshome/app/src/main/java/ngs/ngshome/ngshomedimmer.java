package ngs.ngshome;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class ngshomedimmer extends Activity {

    private SeekBar 		dimmercontrol;
    private TextView		progress;
    private ToggleButton	dimmeractivate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ngshomedimmer);

        dimmercontrol 			= (SeekBar)  		findViewById(R.id.dimmercontrol);
        dimmeractivate			= (ToggleButton)	findViewById(R.id.dimmeractivate);
        progress 				= (TextView) 		findViewById(R.id.progress );

        progress.setText(Integer.toString(dimmercontrol.getProgress()));

        mqttconnector.GetInstance().Connect();

        mqttconnector.GetInstance().Subscribe("dimmer.control.update");

        dimmeractivate.setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v)
                    {
                        if ( View.VISIBLE == dimmercontrol.getVisibility() )
                        {
                            dimmercontrol.setVisibility(View.INVISIBLE);
                            mqttconnector.GetInstance().Publish("dimmer.control.activate", Integer.toString(0) );
                        }
                        else
                        {
                            dimmercontrol.setVisibility(View.VISIBLE);
                            mqttconnector.GetInstance().Publish("dimmer.control.activate", Integer.toString(1) );
                        }

                    }

                });
        dimmercontrol.setOnSeekBarChangeListener(
                new OnSeekBarChangeListener() {

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser)
                    {
                        progress.setText(Integer.toString(progresValue));

                        mqttconnector.GetInstance().Publish("dimmer.control.value", Integer.toString(progresValue) );
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar)
                    {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar)
                    {

                    }
                } );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ngshomedimmer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
