package ngs.ngshome;

import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * Created by Andrei Benchea on 10/17/2014.
 */
public class mqttconnector implements MqttCallback {

    private static  mqttconnector 	instance;

    private         MqttClient		client;

    public static mqttconnector GetInstance( )
    {
        if ( null == instance )
        {
            instance = new mqttconnector();
        }


        return instance;
    }

    public Boolean Connect( )
    {
        try
        {
            instance.client = new MqttClient("tcp://192.168.0.100:1883", UUID.randomUUID().toString().substring(0, 10), new MemoryPersistence() );

            MqttConnectOptions conOptions = new MqttConnectOptions();

            conOptions.setCleanSession(Boolean.TRUE);

            instance.client.setCallback( instance );

            instance.client.connect(conOptions);

        }
        catch (Exception e)
        {
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    public Boolean Subscribe( String topic )
    {
        try
        {
            client.subscribe(topic, 1);
        }
        catch (Exception e)
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Boolean Publish( String topic, String message )
    {
        MqttMessage mqttmessage = new MqttMessage();

        mqttmessage.setPayload( message.getBytes() );
        mqttmessage.setQos(1);
        mqttmessage.setRetained(Boolean.FALSE);

        try
        {
            client.publish( topic, mqttmessage);
        }
        catch (Exception e)
        {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public void connectionLost(Throwable arg0)
    {
        Connect();
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken arg0)
    {

    }

    @Override
    public void messageArrived(String arg0, MqttMessage arg1) throws Exception
    {

    }

    private mqttconnector()
    {

    }
}
